from sqlalchemy import create_engine
import psycopg2
import psycopg2.extras
import os
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT


table_order = {
    "llt":"pt",
    "pt":"hlt",
    "hlt":"hlgt",
    "hlgt":"soc"
}

tables ={
    "llt":{
        "above":"pt",
        "below":None,
        "id_var":"llt_code"
    },
    "pt":{
        "above":"hlt_pt",
        "below":None,
        "id_var":"pt_code"
    },
    "hlt":{
        "above":"hlgt_hlt",
        "below":"hlt_pt",
        "id_var":"hlt_code"
    },
    "hlgt":{
        "above":"soc_hlgt",
        "below":"hlgt_hlt",
        "id_var":"hlgt_code"
    },
    "soc":{
        "above":None,
        "below":"soc_hlgt",
        "id_var":"soc_code"
    },
}

DB_HOST = os.environ.get('postgresHost',"localhost")
DB_PASSWORD=  os.environ.get('postgresPassword',"postgres")
DB_USER = os.environ.get('postgresUser',"postgres")
DB_PORT = os.environ.get('postgresPost',"5432")
def search_for_term(db_name, search_term, table, traverse):
    try:
        conn = psycopg2.connect("dbname="+db_name+" user="+DB_USER+" password="+DB_PASSWORD+" host="+DB_HOST+" port="+DB_PORT)
        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT);
        cur = conn.cursor(cursor_factory = psycopg2.extras.DictCursor)
    except (Exception, psycopg2.DatabaseError) as error:
        print(error,flush=True)
        return {"statusCode": 500, "message": "Failed to access client DB"}
    try :
        command = f"select * from {table} where SIMILARITY({table}_name,'%{search_term}%') > 0.41 or {table}_name @@ '{search_term}';"
        cur.execute(command)
    except Exception as e:
        if ('pgcode' in dir(e) and e.pgcode == "42883"):
            print("Could not do SIMILARITY, creating extension pg_trgm coz extension was not found")
            command = "CREATE EXTENSION pg_trgm;"
            cur.execute(command)
            search_for_term(db_name, search_term, table, traverse)
        else :
            return {"message":e, "statusCode":500}
    res=[]
    for ob in cur:
        res.append(dict(ob))
    if len(res)>0:
        return {"table":table, "data":res, "statusCode":200}
    elif traverse:
        if table != "soc":
            return search_for_term(db_name, search_term, table_order[table],traverse)
        else :
            return {"message":"Not found", "statusCode":400}
    else:
        return {"message":"Not found in that table", "statusCode":400}


def add_next(db_name, table, data):
    try:
        conn = psycopg2.connect("dbname="+db_name+" user="+DB_USER+" password="+DB_PASSWORD+" host="+DB_HOST+" port="+DB_PORT)
        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT);
        cur = conn.cursor(cursor_factory = psycopg2.extras.DictCursor)
    except (Exception, psycopg2.DatabaseError) as error:
        print(error,flush=True)
        print("here",flush=True)
        return {"statusCode": 500, "message": "Failed to access client DB"}, 500

    tab_rels = tables[table]
    above_table = "N/A"
    above_ids=[]
    above_data = []
    ret_list=[]
    if tab_rels["above"] is not None:
        above_table = tab_rels["above"].split("_")
        above_table= above_table[0]
    new_data = []
    if table == "llt":
        table = "pt"
    if above_table != "N/A":
        for i in data:
            # print(i)
            search_id = i[f"{table}_code"]
            command = f"select {above_table}_code from {tab_rels['above']} where {table}_code LIKE '%{search_id}%';"
            cur.execute(command)
            for j in cur:
                above_data.append(j[f"{above_table}_code"])
            for dat in above_data:
                command = f"select * from {above_table} where {above_table}_code LIKE '%{dat}%'"
                cur.execute(command)
                for c in cur:
                    val = c[f"{above_table}_name"]
                    val_id = c[f"{above_table}_code"]
                    i[f"{above_table}_name"] = val
                    i[f"{above_table}_code"] = val_id

                    new_data.append(i)

        return {"above":above_table, "data":new_data}
    elif above_table == "N/A":
        return {"above":None, "data":data}

def term_search(db_name, term, table, traverse=True):
    a = search_for_term(db_name, term, table, traverse)
    print(a)
    if a["statusCode"] != 200:
        return a
    tab = a["table"]
    final_stuff=[]
    for i in a["data"]:
        if tab == "soc":
            final_stuff.extend(a["data"])
        else:  
            data=add_next(db_name, tab, [i])

            above=data["above"]
            core = data["data"]
            while(above != None):
                data=add_next(db_name, above, core)
                above=data["above"]
                core = data["data"]
                final_stuff.extend(core)
    # final_stuff2 = [itm for itm in final_stuff if itm not in final_stuff2]
    final_stuff2=[]
    for itm in final_stuff:
        if itm not in final_stuff2:
            final_stuff2.append(itm)
    return ({"statusCode":200, "data":final_stuff2})
