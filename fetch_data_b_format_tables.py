
import psycopg2
import pandas as pd
import collections
import csv
import os
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
import json

DB_HOST = os.environ.get('postgresHost',"localhost")
DB_PASSWORD=  os.environ.get('postgresPassword',"postgres")
DB_USER = os.environ.get('postgresUser',"postgres")
DB_PORT = os.environ.get('postgresPost',"5432")
def arrange_data(rows):
	results = []
	for row in rows:
		output = {"drugName":row[0], "substanceName":row[1],"atcCode":row[2],"ina_text":row[3],"source_code":row[4],"sourceText":row[5],"country_code":row[6],"country_name":row[7] }
		if output not in results:
			results.append(output)
	return results 		
def fetch_records_b_format(search_input,db_name):
	try:
	    conn = psycopg2.connect("dbname="+db_name+" user="+DB_USER+" password="+DB_PASSWORD+" host="+DB_HOST+" port="+DB_PORT)
	    conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT);
	    cur = conn.cursor()
	except (Exception, psycopg2.DatabaseError) as error:
		return {"message": "Database not exists hit api -- createdb "}
	cmd = "DROP TABLE IF EXISTS joined_table"
	cur.execute(cmd)
	query = "create table joined_table as select dd.drugName,bna.substanceName,dda.atcCode,ina.ina_text,bna.source_code,ddsource.source,ccode.country_code,ccode.country_name from dd inner join dda on dd.drug_record_number=dda.drug_record_number and dd.sequence_number_1 = dda.sequence_number_1 and dd.sequence_number_2=dda.sequence_number_2 inner join ing on dd.drug_record_number = ing.drug_record_number and dd.sequence_number_1 =ing.sequence_number_1 and ing.sequence_number_2 = dd.sequence_number_2 inner join ddsource on ddsource.source_code = dd.source_code inner join ccode on ddsource.country_code = ccode.country_code inner join ina on dda.atcCode = ina.atcCode inner join bna on ddsource.source_code = bna.source_code "       
	cur.execute(query)
	cmd = "DROP extension if exists pg_trgm"
	cur.execute(cmd)	
	query = "create extension pg_trgm with schema pg_catalog;"
	cur.execute(query)
	query = "CREATE INDEX trgm_idx ON joined_table USING gin (drugName gin_trgm_ops)"
	cur.execute(query)
	conn.commit()
	final_results =[]
	query= "SELECT * from joined_table  where drugName LIKE '%"+ '%s'%(search_input.upper())+"%'"
	cur.execute(query)
	rws = cur.fetchall()
	results = arrange_data(rws)
	final_results.extend(results)
	query = "SELECT *, similarity(drugName, '"+'%s'%(search_input.upper())+"')  AS sim from joined_table ORDER BY sim DESC limit 15"
	cur.execute(query)
	rows = cur.fetchall()
	results = arrange_data(rows)
	final_results.extend(results)
	return {"data":final_results}
