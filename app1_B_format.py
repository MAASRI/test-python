#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 22 13:44:39 2021

@author: bhavani
"""
import psycopg2
import pandas as pd
import collections
import csv
import os
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
import json
from terms_ordering import terms_search_whodd
from fetch_data_b_format_tables import fetch_records_b_format
from flask import Flask, request
from flask_cors import CORS
app = Flask(__name__)
CORS(app)
DB_HOST = os.environ.get('postgresHost',"localhost")
DB_PASSWORD=  os.environ.get('postgresPassword',"postgres")
DB_USER = os.environ.get('postgresUser',"postgres")
DB_PORT = os.environ.get('postgresPost',"5432")
def txt_to_csv(folder_to_extract_path,folder_name):
    dd = open(str(folder_to_extract_path) + '/' + 'Dd.txt', 'r').readlines()
    Drug_Record_Number = []
    Sequence_Number_1 = []
    Sequence_Number_2 = []

    Check_Digit = []
    Designation = []
    Source_Year = []
    Source_Code = []
    Company_Code = []
    Number_of_Ingredients = []
    Salt_ester_Code = []
    Year_quarter = []
    Drug_Name = []
# changed positions & added Pk's and FK's to each table as like in pdf
    for el in dd:

        Drug_Record_Number.append(el[0:6].strip())
        Sequence_Number_1.append(el[6:8].strip())
        Sequence_Number_2.append(el[8:11].strip())
        # Drug_code.append(el[0:11].strip())
        Check_Digit.append(el[11].strip())
        Designation.append(el[12].strip())
        Source_Year.append(el[13:15].strip())
        Source_Code.append(el[15:19].strip())
        Company_Code.append(el[19:24].strip())
        Number_of_Ingredients.append(el[24:26].strip())
        Salt_ester_Code.append(el[26].strip())
        Year_quarter.append(el[27:30].strip())
        if '/' in el[30:75]:
            drugName = el[30:75].replace('/','')
            drugName = ''.join([i for i in drugName if not i.isdigit()])
        else:
            drugName  = el[30:75]
        Drug_Name.append(drugName.strip())
    dd = pd.DataFrame()
    dd['Drug_Record_Number'] = Drug_Record_Number
    dd['Sequence_Number_1'] = Sequence_Number_1
    dd['Sequence_Number_2'] = Sequence_Number_2
    dd['Check_Digit'] = Check_Digit
    dd['Designation'] = Designation
    dd['Source_Year'] = Source_Year
    dd['Source_Code'] = Source_Code
    dd['Company_Code'] = Company_Code
    dd['Number_of_Ingredients'] = Number_of_Ingredients
    dd['Salt_ester_Code'] = Salt_ester_Code
    dd['Year_quarter'] = Year_quarter
    dd['Drug_Name'] = Drug_Name
    dd.to_csv(str(folder_to_extract_path) + '/' + 'Dd.csv', header = False, index=False,sep= '\t')
    dd = open(str(folder_to_extract_path) + '/' + 'Bna.txt', 'r').readlines()
    CAS_number= []
    Language_code=[]
    Substance_name=[]
    Source_year=[]
    Source_code=[]
    for i in dd:
        CAS_number.append(i[0:10].strip())
        Language_code.append(i[10:12].strip())
        Substance_name.append(i[12:57].strip())
        Source_year.append(i[57:59].strip())
        Source_code.append(i[59:63].strip())
        
    dd = pd.DataFrame()
    dd['CAS_number'] =CAS_number
    dd['Language_code'] = Language_code
    dd['Substance_name'] = Substance_name
    dd['Source_year'] =Source_year
    dd['Source_code'] =   Source_code
    dd.to_csv(str(folder_to_extract_path) + '/' + 'Bna.csv', header = False, index=False,sep= '\t')
    dd = open(str(folder_to_extract_path) + '/' + 'Ccode.txt', 'r').readlines()
    Country_code= []
    Country_name=[]
    for i in dd:
        Country_code.append(i[0:3].strip())
        Country_name.append(i[3:33].strip())
        
    dd = pd.DataFrame()
    dd['Country_code'] =Country_code
    dd['Country_name'] = Country_name
    dd.to_csv(str(folder_to_extract_path) + '/' + 'Ccode.csv', header = False, index=False,sep= '\t')
    dd = open(str(folder_to_extract_path) + '/' + 'Dda.txt', 'r').readlines()
    Drug_Record_Number = []
    Sequence_Number_1 = []
    Sequence_Number_2 = []
    Check_Digit = []
    ATC_code = []
    Year_quarter = []
    Official_ATC_code = []
    
    for el in dd:
        Drug_Record_Number.append(el[0:6].strip())
        Sequence_Number_1.append(el[6:8].strip())
        Sequence_Number_2.append(el[8:11].strip())

        Check_Digit.append(el[11].strip())
        ATC_code.append(el[12:19].strip())
        Year_quarter.append(el[19:22].strip())
        Official_ATC_code.append(el[22].strip())
    
    dd = pd.DataFrame()
    dd['Drug_Record_Number'] = Drug_Record_Number
    dd['Sequence_Number_1'] = Sequence_Number_1
    dd['Sequence_Number_2'] = Sequence_Number_2
    # dd['Drug_code']=Drug_code
    dd['Check_Digit'] = Check_Digit
    dd['ATC_code'] = ATC_code
    dd['Year_quarter'] = Year_quarter
    dd['Official_ATC_code'] = Official_ATC_code
    dd.to_csv(str(folder_to_extract_path) + '/' + 'Dda.csv', header = False, index=False,sep= '\t')
    dd = open(str(folder_to_extract_path) + '/' + 'Ina.txt', 'r').readlines()
    ATC_code= []
    Level= []
    Text= []
    
    for i in dd:
        ATC_code.append(i[0:7].strip())
        Level.append(i[7].strip())
        Text.append(i[8:58].strip())
    
        
    dd = pd.DataFrame()
    
    dd['ATC_code'] =ATC_code
    dd['Level'] =Level
    dd['Text'] = Text
    dd.to_csv(str(folder_to_extract_path) + '/' +'Ina.csv', header = False, index=False,sep= '\t')
    dd = open(str(folder_to_extract_path) + '/' + 'Ing.txt', 'r').readlines()
    Drug_Record_Number = []
    Sequence_Number_1 = []
    Sequence_Number_2 = []
    Check_Digit= []
    CAS_number= []
    Drug_code=[]
    for i in dd:
        # print(len(i),i[21])
        # Drug_code.append(i[0:11].strip())
        Drug_Record_Number.append(i[0:6].strip())
        Sequence_Number_1.append(i[6:8].strip())
        Sequence_Number_2.append(i[8:11].strip())
        Check_Digit.append(i[11].strip())
        CAS_number.append(i[12:22].strip())
        
    dd = pd.DataFrame()
    dd['Drug_Record_Number'] =Drug_Record_Number
    dd['Sequence_Number_1'] =Sequence_Number_1
    dd['Sequence_Number_2'] = Sequence_Number_2
    # dd['Drug_code']=Drug_code
    dd['Check_Digit'] =Check_Digit
    dd['CAS_number'] =   CAS_number
    dd.to_csv(str(folder_to_extract_path) + '/' +'Ing.csv', header = False, index=False,sep= '\t')
    dd = open(str(folder_to_extract_path) + '/' + 'Man.txt', 'r').readlines()
    Company_code = []
    Name = []
    Country_code = []
    for i in dd:
        Company_code.append(i[0:5].strip())
        Name.append(i[5:75].strip())
        Country_code.append(i[75:78].strip())
    dd = pd.DataFrame()
    dd['Company_code'] = Company_code
    dd['Name'] = Name
    dd['Country_code'] = Country_code
    dd.to_csv(str(folder_to_extract_path) + '/' +'Man.csv', header = False, index=False,sep= '\t')
    dd = open(str(folder_to_extract_path) + '/' + 'Ddsource.txt', 'r').readlines()
    Source_code= []
    Source= []
    Country_code= []
    
    for i in dd:
        Source_code.append(i[0:4].strip())
        Source.append(i[4:74].strip())
        Country_code.append(i[74:78].strip())
    
        
    dd = pd.DataFrame()
    
    dd['Source_code'] =Source_code
    dd['Source'] =Source
    dd['Country_code'] = Country_code
    dd.to_csv(str(folder_to_extract_path) + '/' +'Ddsource.csv', header = False, index=False,sep= '\t')
def create_db(db_name):

    conn = None
    try:
        # read the connection parameters
        # params = config()
        # connect to the PostgreSQL server
        conn = psycopg2.connect("user="+DB_USER+" password="+DB_PASSWORD+" host="+DB_HOST+" port="+DB_PORT)
        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT);
        cur = conn.cursor()
        command = "DROP DATABASE IF EXISTS \"" + db_name + "\";"
        cur.execute(command)
        # print('yes')
        command = "CREATE DATABASE \"" + db_name + "\";"
        cur.execute(command)
    
	# close communication with the PostgreSQL database server
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)

    finally:
	    if conn is not None:
	        conn.close()
def create_tables(folder_to_extract_path,db_name):
    """ create tables in the PostgreSQL database"""
    commands =[

    "create table ccode(country_code VARCHAR(255),country_name VARCHAR(255), primary key (country_code));",
    "create table man (company_code VARCHAR(255),name VARCHAR(255),country_code VARCHAR(255),primary key (company_code), foreign key(country_code) references ccode (country_code)  );" ,
    "create table ddsource (source_code VARCHAR(255),source VARCHAR(255),country_code VARCHAR(255),primary key (source_code), foreign key(country_code) references ccode(country_code));", 
    "create table ina (atcCode VARCHAR(255),level VARCHAR(255),ina_text VARCHAR(255),primary key (atcCode));",
    "create table bna (cas_number VARCHAR(255),language_code VARCHAR(255),substanceName VARCHAR(255),source_year VARCHAR(255),source_code VARCHAR(255));",
    "create table dd (drug_record_number VARCHAR(255),sequence_number_1 VARCHAR(255),sequence_number_2 VARCHAR(255),check_digit VARCHAR(255),designation VARCHAR(255),source_year VARCHAR(255),source_code VARCHAR(255),company_code VARCHAR(255),number_of_iigredients VARCHAR(255),salt_ester_code VARCHAR(255),year_quarter VARCHAR(255),drugName VARCHAR(255), primary key(drug_record_number,sequence_number_1,sequence_number_2), foreign key (source_code) references ddsource(source_code));", 
    "create table dda (drug_record_number VARCHAR(255),sequence_number_1 VARCHAR(255),sequence_number_2 VARCHAR(255),check_digit VARCHAR(255),atcCode VARCHAR(255),year_quarter VARCHAR(255),official_atc_code VARCHAR(255),primary key(drug_record_number,sequence_number_1,sequence_number_2,atcCode),foreign key (atcCode) references ina(atcCode));",  
    "create table ing (drug_record_number VARCHAR(255),sequence_number_1 VARCHAR(255),sequence_number_2 VARCHAR(255),check_digit VARCHAR(255) ,cas_number VARCHAR(255) , primary key (drug_record_number,sequence_number_1,sequence_number_2,cas_number) );"
    
    ]
    try:
        conn = psycopg2.connect("dbname="+db_name+" user="+DB_USER+" password="+DB_PASSWORD+" host="+DB_HOST+" port="+DB_PORT)
        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT);
        cur = conn.cursor()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error,flush=True)
        print("here",flush=True)
        #rr.rrAdapter("warning",  "Failed to access client DB", {"rrId":str(uuid.uuid4()), "source":"Medictionary Service"})
        return {"statusCode": 500, "message": "Failed to access client DB"}, 500
    names = ['Ccode','Man', 'Ddsource','Ina','Bna','Dd','Dda','Ing'] 
    for cmds in commands:
        i = commands.index(cmds)
        print(names[i])
        cur = conn.cursor()

        cur.execute((cmds))
        # print('created',cmds,names[i])
        file = folder_to_extract_path+'/'+names[i]+".csv"
        print(file)

        f = open(file, 'r')
        cur.copy_from(f, names[i].lower(), sep= '\t', null="")
        f.close()
        conn.commit()


    cur.close()


@app.route("/b_format/createdb",methods=['POST'])
def create_database():
    user_data = json.loads(request.data)
    folder_name = user_data['folder_name']
    folder_to_extract_path = str(folder_name) + '/' + "Sample B2 Format"
    txt_to_csv(folder_to_extract_path,folder_name)
#implemented postgresql
    # db_name = user_data['client_id']
    db_name = "whodd_b_format" 
    create_db(db_name)
    create_tables(folder_to_extract_path,db_name)
    return {"message":"database and tables successfully created."}
@app.route("/b_format/search_drug", methods = ['POST'])
def search_api():
    user_data = json.loads(request.data)
    # db_name = user_data['client_id']
    db_name = "whodd_b_format"
    search_term = user_data['drugName']
    final_results = fetch_records_b_format(search_term,db_name)
    final_results = terms_search_whodd(final_results,search_term,db_name,'b')
    return final_results

