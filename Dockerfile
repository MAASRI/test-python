FROM python:3.6

ADD ./req.txt .
ADD ./app1.py .
ADD ./test_app1.py .
ADD ./new_search.py .
ADD ./app1_B_format.py .
ADD ./app1_C_fomat.py .
ADD ./WhoDD ./WhoDD
ADD ./fetch_data_b_format_tables.py .
ADD ./fetch_data_c_format_tables.py .
ADD ./terms_ordering.py .
ADD ./terms_search_ordering.py .
ADD ./cosine_similarity.py .

EXPOSE 8008

RUN pip3 install -r req.txt

CMD ["python","-u","app1.py"]
