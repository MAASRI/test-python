Steps to run api:
Execute app1.py file
Hit the url in postman:http://0.0.0.0:8008/searchall_extension

inpute parameters:{
    "clientId": "meddra_code1",
   "search_term":"flu",
   "version":"23.0",
   "table":"llt"
    }
output response: Realated meddra terms for a given search term in an order sorted based on similarity score.
Output file: searchterm_searchall_extension.txt file will generate( ex: user searches flu . flu_searchall_extension.txt)

WHO-DD Details:
******B-Format******************
run 'app1_B_format.py'
1)API: http://0.0.0.0:9008/createdb

API request params:    {"folder_name":"./WhoDD", "clientId" : "whodd_b_format"}
O/p response: created db and tables

2) API : http://0.0.0.0:9008/search_drug

API request params:   {"drugName": "paracetamol", "clientId" : "whodd_b_format"}
O/P response: check the response in postman	


***************C-format***********************
run 'app1_C_format.py'
1) API: /c_format/createdb

API request params:    {"folder_name":"./WhoDD", "clientId" : "whodd_c_format"}
O/p response: created db and tables

2) API : /c_format/search

********i/p keys***
search by exact match: medicinalProdId, medId,atcCode (Only one allowed to be search at a time)
search by text : drugName,sourceText,substanceName,mahName (Only one allowed to be search at a time)
search by group: country,sourceCountry,productGroupName,productTypeText,route,pharmaceuticalFormText,mahOrgCountry (can be search Any combination)

API request params:   {"medicinalProdId":4972, "clientId" : "whodd_c_format"}
O/P response: check the response in postman



3)API: /c_format/list

API request params:    {"what":"all", "clientId" : "whodd_c_format"}
response : list of items..from db eg: country list , product_group list...

*****'what' keys :'country','sourceCountry',"mahCountry","productGroup","productType","pharmacetucalForm","route"( you can pass any of these item**)

http://0.0.0.0:8008/list?what=country
response: {country list :[
							{ label: "India", value: "India" },
        					{ label: "Japan", value: "Japan" },
        					{ label: "Germany", value: "Germany" }....

        				]

        	}
