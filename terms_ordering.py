
#importing required packages
import nltk
from nltk import ngrams
# from new_search import new_term_search
from nltk.stem import WordNetLemmatizer 
import json
import os
import cosine_similarity
from nltk import word_tokenize
import re
from fetch_data_c_format_tables import fetch_records_drug
from fetch_data_b_format_tables import fetch_records_b_format
wnl = WordNetLemmatizer() 
def terms_search_whodd(a,search_term,db_name,format):

	data = []

	#Recomendation based on parent group 
	x = a['data']
	#drug names
	drugNames = [i['drugName'] for i in x]
	#(collecting all substanceNames)
	substanceNames = [i['substanceName'] for i in x]
	#if pt_name not in llt name then again sent for getting related terms
	final_substanceNames = [i for n, i in enumerate(substanceNames) if i not in substanceNames[n + 1:]]
	for i in range(len(final_substanceNames )):
		if final_substanceNames [i] not in drugNames:
			print("getting...related terms of..",final_substanceNames [i])
			if "'" in final_substanceNames[i]:
				final_substanceNames[i] = final_substanceNames[i].replace("'", '')
			if format == 'b':
				a = fetch_records_b_format(final_substanceNames[i],db_name)
			elif format == 'c':
				print("yeah..............")
				a = fetch_records_drug(final_substanceNames[i],db_name)
			try:
				x.extend(a['data'])
			except:
				pass
		else:
			# print("already  exists",final_pt_names [i])
			continue

	#makes final list of terms
	a_list = [i for n, i in enumerate(x) if i not in x[n + 1:]]
	#get similarity scores    folder_name = "/home/bhavani/Desktop/WhoDD 2/WhoDD"
	data = similarity_score(a_list,search_term)	

	#composite score of llt and pt similarity scores.
	for i in range(len(data)):
		print(data[i])
		data[i]['similarityScore'] = (80*data[i]['drugSimilarityScore']+20*data[i]['substanceSimilarityScore'])/100
		# if table == 'dd':
		# 	data[i]['similarityScore'] = (80*data[i]['llt_similarity_score']+20*data[i]['pt_similarity_score'])/100

		# else:
		# 	data[i]['similarityScore'] = (data[i]['pt_similarity_score']+data[i]['llt_similarity_score'])/2
	#sorting based on similarity score
	# data = sorted(data, key = lambda i: i['llt_similarity_score'],reverse=True)
	data = sorted(data, key = lambda i: i['similarityScore'],reverse=True)
	#saving output response
	return ({"data":data})


def similarity_score(a,search_term):	
	a = get_similarity_scores(a,search_term,'drugName')
	a = get_similarity_scores(a,search_term,'substanceName')		
	return a	


def get_similarity_scores(a,search_term,name):
	search_term = search_term.lower()
	#lemmatization....
	list2 = nltk.word_tokenize(search_term) 
	lemmatized_string = ' '.join([wnl.lemmatize(words) for words in list2])

	search_term = lemmatized_string

	word_bi_gram_result,character_tri_gram_result = char_word_n_grams(search_term)

	for i in range(len(a)):
		if "'" in a[i][name]:
			term = a[i][name].replace("'", '')
		else:
			term = a[i][name]
		list2 = nltk.word_tokenize(term.lower()) 
		lemmatized_string = ' '.join([wnl.lemmatize(words) for words in list2])
		term = lemmatized_string

		#multiple words in a search term(Blood acid phosphotase increased, Aspiration pleural cavity ..etc..)
		if len(word_bi_gram_result) != 0:
			


			# matching_count_word = [count+1 if x.lower() in a[i][name].lower() else count  for x in word_bi_gram_result]

			field_terms = word_tokenize(term)
			searched_words =  word_tokenize(search_term)
			count = 0

			matching_count_word = [count+1 if field_terms==searched_words or list(field_terms) == list(reversed(searched_words)) else cosine_similarity.get_result(x,term)  for x in word_bi_gram_result]
			count1 = 0
			matching_count_char = [count1+1 if x in term else count1 for x in character_tri_gram_result ]
			count_word = sum(matching_count_word)
			count_char = sum(matching_count_char)
			char_similarity = count_char/len(character_tri_gram_result)
			word_similarity = count_word/len(word_bi_gram_result)
			#80% of word similarity and 20% of char similarity
			similarity_score = (80*word_similarity+20*char_similarity)/100
			a[i]['%sSimilarityScore'%(name[:-4])] = similarity_score
		#single word in search terrm(flu, pain .....etc.)
		else:
			# print(term,search_term)
			word_similarity = 	cosine_similarity.get_result(term,search_term)
			count1 = 0
			matching_count_char = [count1+1 if x in term else count1 for x in character_tri_gram_result ]
			count_char = sum(matching_count_char)
			char_similarity = count_char/len(character_tri_gram_result)
			similarity_score = (80*word_similarity+20*char_similarity)/100
			# a[i]['%ssimilarity_score'%(name[:-4])] = char_similarity
			a[i]['%sSimilarityScore'%(name[:-4])] = similarity_score

	return a

def char_word_n_grams(search_term):
	# word bigram
	n = 2
	bi_grams = ngrams(search_term.split(), n)
	word_bi_gram_result = [ ' '.join(grams) for grams in bi_grams]

	if '(' in search_term:

# ex: Rhesus antibodies (anti-D) --> word_bi_gram_result = ['rhesus antibody', 'antibody (', '( anti-d', 'anti-d )'] to make it correct -->['rhesus antibody', 'antibody (anti-d)']' 
		word_bigram_result1 = correct_bigrams(word_bi_gram_result)
	else:
		word_bigram_result1 = word_bi_gram_result



	#character trigram
	n1 = 3
	character_tri_gram_result= [search_term[i:i+n1] for i in range(len(search_term)-n1+1) ]

	character_tri_gram_result1 = [x for x in character_tri_gram_result if ' ' not in x]
	return word_bigram_result1,character_tri_gram_result1

def correct_bigrams(word_bi_gram_result):
	fianl_words_list = []
	for i in range(len(word_bi_gram_result)):
	    if '(' in word_bi_gram_result[i]:
	        j = i+1
	        try:
	            while j:
	                if ')' in word_bi_gram_result[j]:
	                    if word_bi_gram_result[j].replace(')','').strip() != word_bi_gram_result[i].replace('(','').strip():
	                        word_bi_gram_result[i] = word_bi_gram_result[i]+" "+word_bi_gram_result[j]
	                        fianl_words_list.append(word_bi_gram_result[i])
	                    break
	                else:
	                    j = j+1
	                    if j == (len(word_bi_gram_result)):
	                        break
	        except:
	            pass
	    elif ')' not in  word_bi_gram_result[i] :
	        fianl_words_list.append(word_bi_gram_result[i])

	# word_bi_gram_result = ['rhesus antibody', 'antibody (anti-d)']
	for i  in range(len(fianl_words_list)):

	    if fianl_words_list[i].count(')') >1:

	        attach_word = re.search(r'\(.*?\)',fianl_words_list[i-1])
	        if attach_word:
		        attach_word = attach_word.group(0).replace(')','')

		        fianl_words_list[i] = str(attach_word)+" "+fianl_words_list[i]
	return fianl_words_list