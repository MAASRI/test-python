#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 22 13:44:39 2021

@author: bhavani
"""
import psycopg2
import pandas as pd
import collections
import csv
import os
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
import json
from terms_ordering import terms_search_whodd
from fetch_data_c_format_tables import joining_and_fetching_data
from flask import Flask, request
from flask_cors import CORS
from fetch_data_c_format_tables import show_dropdown_data
app = Flask(__name__)
CORS(app)
DB_HOST = os.environ.get('postgresHost',"localhost")
DB_PASSWORD=  os.environ.get('postgresPassword',"postgres")
DB_USER = os.environ.get('postgresUser',"postgres")
DB_PORT = os.environ.get('postgresPost',"5432")
def txt_to_csv(folder_to_extract_path,folder_name):
    dd = open(str(folder_to_extract_path) + '/' + 'Mp.txt', 'r').readlines()
    medicinalProdId = []
    MedID = []
    Drug_Record_Number =[]
    Sequence_Number_1 =[]
    Sequence_Number_2 = []
    Sequence_Number_3 = []
    Sequence_Number_4 = []
    Generic = []
    drugName = []
    Name_Specifier = []
    Marketing_Authorization_Number = []
    Marketing_Authorization_Date = []
    Marketing_Authorization_Withdrawal_Date = []
    Country = []
    Company = []
    Marketing_Authorization_Holder = []
    Source_Code = []
    sourceCountry = []
    Source_Year = []
    Product_Type = []
    Product_Group = []
    Create_Date = []
    Date_Changed = []
   
    for el in dd:
        medicinalProdId.append(el[0:10].strip())
        # print(el[10:44].strip())
        MedID.append(el[10:45].strip())
        Drug_Record_Number.append(el[45:51].strip())
        Sequence_Number_1.append(el[51:53].strip())
        Sequence_Number_2.append(el[53:56].strip())
        Sequence_Number_3.append(el[56:66].strip())
        Sequence_Number_4.append(el[66:76].strip())
        try:
            Generic.append(el[76].strip())
        except:
            Generic.append("Null")
        drugName.append(el[77:157].strip())
        Name_Specifier.append(el[157:187].strip())
        Marketing_Authorization_Number.append(el[187:217].strip())
        Marketing_Authorization_Date.append(el[217:225].strip())
        Marketing_Authorization_Withdrawal_Date.append(el[225:233].strip())
        Country.append(el[233:243].strip())
        Company.append(el[243:253].strip())
        Marketing_Authorization_Holder.append(el[253:263].strip())
        Source_Code.append(el[263:273].strip())
        sourceCountry.append(el[273:283].strip())
        Source_Year.append(el[283:286].strip())
        Product_Type.append(el[286:296].strip())
        Product_Group.append(el[296:306].strip())
        Create_Date.append(el[306:314].strip())
        Date_Changed.append(el[314:322].strip())
    dd = pd.DataFrame()
    dd['medicinalProdId'] =medicinalProdId
    dd['MedID'] = MedID
    dd['Drug_Record_Number'] =Drug_Record_Number
    dd['Sequence_Number_1'] = Sequence_Number_1
    dd['Sequence_Number_2'] = Sequence_Number_2
    dd['Sequence_Number_3'] = Sequence_Number_3
    dd['Sequence_Number_4'] = Sequence_Number_4
    dd['Generic'] = Generic
    dd['drugName'] = drugName
    dd['Name_Specifier'] = Name_Specifier
    dd['Marketing_Authorization_Number'] = Marketing_Authorization_Number
    dd['Marketing_Authorization_Date'] = Marketing_Authorization_Date
    dd['Marketing_Authorization_Withdrawal_Date'] = Marketing_Authorization_Withdrawal_Date
    dd['Country'] = Country
    dd['Company'] = Company
    dd['Marketing_Authorization_Holder'] = Marketing_Authorization_Holder
    dd['Source_Code'] = Source_Code
    dd['sourceCountry'] = sourceCountry
    dd['Source_Year'] = Source_Year
    dd['Product_Type'] = Product_Type
    dd['Product_Group'] = Product_Group
    dd['Create_Date'] = Create_Date
    dd['Date_Changed'] = Date_Changed
   
    dd.to_csv(str(folder_to_extract_path) + '/' + 'mp.csv', header = False, index=False,sep= '\t')
    
    
    dd = open(str(folder_to_extract_path) + '/' + 'Pp.txt', 'r').readlines()
    Pharmproduct_Id = []
    Pharmaceutical_Form=[]
    Route_of_Administration=[]
    medicinalProdId=[]
    Number_of_Ingredients=[]
    Create_Date = []
    for i in dd:
        Pharmproduct_Id.append(i[0:10].strip())
        Pharmaceutical_Form.append(i[10:20].strip())
        Route_of_Administration.append(i[20:30].strip())
        medicinalProdId.append(i[30:40].strip())
        Number_of_Ingredients.append(i[40:42].strip())
        Create_Date.append(i[42:50].strip())
        
    dd = pd.DataFrame()
    dd['Pharmproduct_Id'] = Pharmproduct_Id
    dd['Pharmaceutical_Form']=Pharmaceutical_Form
    dd['Route_of_Administration']=Route_of_Administration
    dd['medicinalProdId']=medicinalProdId
    dd['Number_of_Ingredients']=Number_of_Ingredients
    dd['Create_Date'] = Create_Date
    dd.to_csv(str(folder_to_extract_path) + '/' + 'pp.csv', header = False, index=False,sep= '\t')
    
    
    dd = open(str(folder_to_extract_path) + '/' + 'Thg.txt', 'r').readlines()
    Therapgroup_Id = []
    ATCCode  = []
    Create_Date = []
    Official_atcCode = []
    medicinalProdId = []
    for el in dd:
        Therapgroup_Id.append(el[0:10].strip())
        ATCCode.append(el[10:20].strip())
        Create_Date.append(el[20:28].strip())
        try:
            Official_atcCode.append(el[28])
        except:
            Official_atcCode.append('Null')
        medicinalProdId.append(el[29:39].strip())
    dd = pd.DataFrame()
    dd['Therapgroup_Id'] = Therapgroup_Id
    dd['ATCCode']   = ATCCode 
    dd['Create_Date']  = Create_Date
    dd['Official_atcCode'] = Official_atcCode
    dd['medicinalProdId'] = medicinalProdId
    dd.to_csv(str(folder_to_extract_path) + '/' + 'thg.csv', header = False, index=False,sep= '\t')
    
    
    dd = open(str(folder_to_extract_path) + '/' + 'ing.txt', 'r').readlines()
    Ingredient_Id = []
    Create_Date = []
    Substance_Id = []
    Quantity = []
    Quantity_2 = []
    Unit= []
    Pharmproduct_Id= []
    medicinalProdId= []
    
    
    for el in dd:
        Ingredient_Id.append(el[0:10].strip())
        Create_Date.append(el[10:18].strip())
        Substance_Id.append(el[18:28].strip())
        Quantity.append(el[28:43].strip())
        Quantity_2.append(el[43:58].strip())
        Unit.append(el[58:68].strip())
        Pharmproduct_Id.append(el[68:78].strip())
        medicinalProdId.append(el[78:88].strip())
    
    
    dd = pd.DataFrame()
    dd['Ingredient_Id'] = Ingredient_Id
    dd['Create_Date'] = Create_Date
    dd['Substance_Id'] = Substance_Id
    dd['Quantity'] =  Quantity
    dd['Quantity_2'] =  Quantity_2
    dd['Unit'] = Unit
    dd['Pharmproduct_Id'] = Pharmproduct_Id
    dd['medicinalProdId'] = medicinalProdId
    
    dd.to_csv(str(folder_to_extract_path) + '/' + 'ing.csv', header = False, index=False,sep= '\t')
    
    
    dd = open(str(folder_to_extract_path) + '/' + 'Srce.txt', 'r').readlines()
    Source_Code = []
    Source = []
    Country_Code = []
    
    for i in dd:
        Source_Code.append(i[0:10].strip())
        Source.append(i[10:90].strip())
        Country_Code.append(i[90:100].strip())
        
    dd = pd.DataFrame()
    
    dd['Source_Code'] = Source_Code
    dd['Source'] =  Source
    dd['Country_Code'] = Country_Code
    dd.to_csv(str(folder_to_extract_path) + '/' +'srce.csv', header = False, index=False,sep= '\t')
    
    
    dd = open(str(folder_to_extract_path) + '/' + 'Org.txt', 'r').readlines()
    Organization_Id = []
    Name = []
    Country_Code= []
    
    for i in dd:
        if i[0:9] == dd[64]:
            break
        else:
            Organization_Id.append(i[0:10].strip())
            Name.append(i[10:90].strip())
            Country_Code.append(i[90:100].strip())
        
        
    dd = pd.DataFrame()
    dd['Organization_Id'] = Organization_Id
    dd['Name'] = Name
    dd['Country_Code'] = Country_Code
    dd.to_csv(str(folder_to_extract_path) + '/' +'org.csv', header = False, index=False,sep= '\t')
    
    dd = open(str(folder_to_extract_path) + '/' + 'ccode.txt', 'r').readlines()
    Country_Code = []
    Country_Name = []

    for i in dd:
        Country_Code.append(i[0:10].strip())
        Country_Name.append(i[10:90].strip())
    dd = pd.DataFrame()
    dd['Country_Code'] = Country_Code
    dd['Country_Name'] = Country_Name
    dd.to_csv(str(folder_to_extract_path) + '/' +'ccode.csv', header = False, index=False,sep= '\t')
    
    dd = open(str(folder_to_extract_path) + '/' + 'Atc.txt', 'r').readlines()
    atcCode= []
    Level= []
    Text= []
    
    for i in dd:
        atcCode.append(i[0:10].strip())
        Level.append(i[10].strip())
        Text.append(i[11:121].strip())
    
        
    dd = pd.DataFrame()
    
    dd['atcCode'] =atcCode
    dd['Level'] =Level
    dd['Text'] = Text

    dd.to_csv(str(folder_to_extract_path) + '/' +'atc.csv', header = False, index=False,sep= '\t')
    
    dd = open(str(folder_to_extract_path) + '/' + 'Sun.txt', 'r').readlines()
    Substance_Id = []
    CAS_number= []
    Language_code=[]
    substanceName=[]
    Source_year=[]
    Source_code=[]
    for i in dd:
        Substance_Id.append(i[0:10].strip())
        CAS_number.append(i[10:20].strip())
        Language_code.append(i[20:30].strip())
        substanceName.append(i[30:140].strip())
        Source_year.append(i[140:143].strip())
        Source_code.append(i[143:153].strip())
        
    dd = pd.DataFrame()
    dd['Substance_Id'] = Substance_Id
    dd['CAS_number'] =CAS_number
    dd['Language_code'] = Language_code
    dd['substanceName'] = substanceName
    dd['Source_year'] =Source_year
    dd['Source_code'] =   Source_code
    dd.to_csv(str(folder_to_extract_path) + '/' +'sun.csv', header = False, index=False,sep= '\t')
    
    dd = open(str(folder_to_extract_path) + '/' + 'Pf.txt', 'r').readlines()
    Pharmform_Id = []
    Text = []
    
    for i in dd:
        Pharmform_Id.append(i[0:10].strip())
        Text.append(i[10:90].strip())
    
        
    dd = pd.DataFrame()
    dd['Pharmform_Id']  = Pharmform_Id
    dd['Text'] =  Text

    dd.to_csv(str(folder_to_extract_path) + '/' +'pf.csv', header = False, index=False,sep= '\t')
    
    dd = open(str(folder_to_extract_path) + '/' + 'Str.txt', 'r').readlines()
    Strength_Id = []
    Text = []
    
    for i in dd:
        Strength_Id.append(i[0:10].strip())
        Text.append(i[10:510].strip())
    
        
    dd = pd.DataFrame()
    dd['Strength_Id'] = Strength_Id
    dd['Text'] =  Text

    dd.to_csv(str(folder_to_extract_path) + '/' +'str.csv', header = False, index=False,sep= '\t')
    
    dd = open(str(folder_to_extract_path) + '/' + 'Prg.txt', 'r').readlines()
    Productgroup_Id = []
    productGroupName= []
    Date_Recorded= []
    
    for i in dd:
        Productgroup_Id.append(i[0:10].strip())
        productGroupName.append(i[10:70].strip())
        Date_Recorded.append(i[70:78].strip())
    
        
    dd = pd.DataFrame()
    
    dd['Productgroup_Id'] =Productgroup_Id
    dd['productGroupName'] =productGroupName
    dd['Date_Recorded'] = Date_Recorded

    dd.to_csv(str(folder_to_extract_path) + '/' +'prg.csv', header = False, index=False,sep= '\t')
    
    dd = open(str(folder_to_extract_path) + '/' + 'Prt.txt', 'r').readlines()
    Prodtype_Id = []
    Text = []
    
    for i in dd:
        Prodtype_Id.append(i[0:10].strip())
        Text.append(i[10:90].strip())
    
        
    dd = pd.DataFrame()
    dd['Prodtype_Id'] = Prodtype_Id
    dd['Text'] =  Text

    dd.to_csv(str(folder_to_extract_path) + '/' +'prt.csv', header = False, index=False,sep= '\t')
    
    dd = open(str(folder_to_extract_path) + '/' + 'UNIT-X.txt',  encoding="utf8", errors='ignore').readlines()
    Unit_Id = []
    Text = []
    
    for i in dd:
        Unit_Id.append(i[0:10].strip())
        Text.append(i[10:50].strip())
    
        
    dd = pd.DataFrame()
    dd['Unit_Id'] = Unit_Id
    dd['Text'] =  Text

    dd.to_csv(str(folder_to_extract_path) + '/' +'unit_x.csv', header = False, index=False,sep= '\t')
    
    dd = open(str(folder_to_extract_path) + '/' + 'UNIT.txt',  encoding="utf8", errors='ignore').readlines()
    Unit_Id = []
    Text = []
    
    for i in dd:
        Unit_Id.append(i[0:10].strip())
        Text.append(i[10:50].strip())
    
        
    dd = pd.DataFrame()
    dd['Unit_Id'] = Unit_Id
    dd['Text'] =  Text

    dd.to_csv(str(folder_to_extract_path) + '/' +'unit.csv', header = False, index=False,sep= '\t')
    
    dd = open(str(folder_to_extract_path) + '/' + 'UNIT-L.txt', encoding="utf8", errors='ignore').readlines()
    Unit_Id = []
    Text = []
    
    for i in dd:
        Unit_Id.append(i[0:10].strip())
        Text.append(i[10:110].strip())
    
        
    dd = pd.DataFrame()
    dd['Unit_Id'] = Unit_Id
    dd['Text'] =  Text

    dd.to_csv(str(folder_to_extract_path) + '/' +'unit_l.csv', header = False, index=False,sep= '\t')

def create_db(db_name):

    conn = None
    try:
        # read the connection parameters
        # params = config()
        # connect to the PostgreSQL server
        conn = psycopg2.connect("user="+DB_USER+" password="+DB_PASSWORD+" host="+DB_HOST+" port="+DB_PORT)
        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT);
        cur = conn.cursor()
        command = "DROP DATABASE IF EXISTS \"" + db_name + "\";"
        cur.execute(command)
        # print('yes')
        command = "CREATE DATABASE \"" + db_name + "\";"
        cur.execute(command)

	# close communication with the PostgreSQL database server
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)

    finally:
	    if conn is not None:
	        conn.close()
def create_tables(folder_to_extract_path,db_name):
    """ create tables in the PostgreSQL database"""
    commands =[

    "create table mp (medicinalProdId VARCHAR(255),medid VARCHAR(255),drug_record_number VARCHAR(255),sequence_number_1 VARCHAR(255),sequence_number_2 VARCHAR(255),sequence_number_3 VARCHAR(255),sequence_number_4 VARCHAR(255),generic VARCHAR(255),drugName VARCHAR(255),name_specifier VARCHAR(255),marketing_authorization_number VARCHAR(255),marketing_authorization_date VARCHAR(255),marketing_authorization_withdrawal_date VARCHAR(255),country VARCHAR(255),company VARCHAR(255),marketing_authorization_holder VARCHAR(255),source_code VARCHAR(255),sourceCountry VARCHAR(255),source_year VARCHAR(255),productType VARCHAR(255),product_group VARCHAR(255),create_date VARCHAR(255),date_changed VARCHAR(255), primary key(medicinalProdId))",
    "create table thg (therapgroup_id VARCHAR(255),atccode  VARCHAR(255),create_date VARCHAR(255),official_atcCode VARCHAR(255),medicinalProdId VARCHAR(255) , foreign key(medicinalProdId) references mp(medicinalProdId))",
    "create table atc (atcCode VARCHAR(255),level VARCHAR(255),atc_text VARCHAR(255))",
    "create table ccode (country_code VARCHAR(255),country_name VARCHAR(255))",
    "create table org (organization_id  VARCHAR(255),name text,country_code VARCHAR(255))",
    "create table srce (source_code  VARCHAR(255),source VARCHAR(255),country_code VARCHAR(255))",
    "create table ing (ingredient_id VARCHAR(255),create_date VARCHAR(255),substance_id VARCHAR(255),quantity VARCHAR(255),quantity_2 VARCHAR(255),unit VARCHAR(255),pharmproduct_id VARCHAR(255),medicinalProdId VARCHAR(255) , foreign key(medicinalProdId) references mp(medicinalProdId))",
    "create table pp (pharmproduct_id VARCHAR(255),pharmaceutical_form VARCHAR(255),route_of_administration VARCHAR(255),medicinalProdId VARCHAR(255),number_of_ingredients VARCHAR(255),create_date VARCHAR(255), foreign key(medicinalProdId) references mp(medicinalProdId))",
    "create table pf (pharmform_id varchar(255),pf_text varchar(255) )",
    "create table prg (productgroup_id VARCHAR(255),productGroupName VARCHAR(255),date_recorded VARCHAR(255))",
    "create table prt (prodtype_id VARCHAR(255),prt_text VARCHAR(255))",
    "create table str (strength_id varchar(255), str_text varchar(255))",
    "create table sun (substance_id varchar(255),cas_number varchar(255),language_code varchar(255),substanceName varchar(255),source_year varchar(255),source_code varchar(255))",
    "create table unit_l (unit_id varchar(255),unit_l_text varchar(255))",
    "create table unit_x (unit_id varchar(255),unit_x_text varchar(255))",
    "create table unit (unit_id varchar(255),unit_text varchar(255))"
    ]
    try:
        conn = psycopg2.connect("dbname="+db_name+" user="+DB_USER+" password="+DB_PASSWORD+" host="+DB_HOST+" port="+DB_PORT)
        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT);
        cur = conn.cursor()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error,flush=True)
        print("here",flush=True)
        #rr.rrAdapter("warning",  "Failed to access client DB", {"rrId":str(uuid.uuid4()), "source":"Medictionary Service"})
        return {"statusCode": 500, "message": "Failed to access client DB"}, 500
    names = ['mp','thg', 'atc','ccode','org','srce','ing','pp','pf','prg','prt','str','sun','unit_l','unit_x','unit'] 
    for cmds in commands:
        i = commands.index(cmds)
        print(names[i])
        cur = conn.cursor()

        cur.execute((cmds))
        # print('created',cmds,names[i])
        file = folder_to_extract_path+'/'+names[i]+'.csv'
        print(file)

        f = open(file, 'r')
        cur.copy_from(f, names[i], sep= '\t', null="")
        f.close()
        conn.commit()
    conn.commit()
    cur.close()
# @app.route("/createdb",methods=['POST'])
# def create_database():
#     user_data = json.loads(request.data)
#     folder_name = user_data['folder_name']
#     folder_to_extract_path = str(folder_name) + '/' + "Sample C Format"
#     txt_to_csv(folder_to_extract_path,folder_name)
# #implemented postgresql
#     db_name = user_data['clientId'] 
#     # db_name = "whodd_c_format"
#     create_db(db_name)
#     create_tables(folder_to_extract_path,db_name)
#     return {"message":"database and tables successfully created. Db-name : %s"%db_name}

# #searchbytext,id,country....
# @app.route("/search", methods = ['POST'])
# def search_api():

#     user_data =json.loads(request.data)
#     print('yesh',user_data)
#     search_by_id = ['medicinalProdId','medId','atcCode']
#     search_by_text = [ "drugName","sourceText","substanceName","mahName"]
#     keys = user_data.keys()
#     if len(set(keys).intersection(search_by_id))>1:
#         return {"message":"search by id : ONLY ONE field allowed to be search at a time"}
#     elif  len(set(keys).intersection(search_by_text))>1:
#         return {"message":"search by text : ONLY ONE field allowed to be search at a time"}
#     else:
#         db_name = user_data['clientId']
#         # db_name = "whodd_c_format"
#         final_results = joining_and_fetching_data(user_data,db_name)
#         if "data" in final_results.keys():
# 	        if 'drugName' in user_data.keys():
# 	        	search_term = user_data['drugName']
# 	        	final_results = terms_search(final_results,search_term,db_name,'c')
# 	        else:
# 	            final_results = final_results
# 	        return final_results
#         else:
#         	return final_results


# #get drop down list items from db
# @app.route("/list",methods = ['POST'])
# def fetch_data_group_fields():
#     # db_name= request.args.get('db_name')
#     db_name = "whodd_c_format"
#     what = request.args.get('what')
#     what_lists = ['country','sourceCountry',"mah_country","product_group","productType","pharmacetucal_form","route"]
#     db_name = "whodd_c_format"
#     country_name,sourceCountry ,product_grp,productType,pf,mah_country,route_of_administrations= show_dropdown_data(db_name)
#     data = {"country list":country_name,"source country list":sourceCountry,"mah_country list":mah_country,"product_group list":product_grp,"productType list":productType,"pharmaceutical_form list":pf,"route list":route_of_administrations}
#     dict_keys = ['country list','source country list' ,'mah_country list','product_group list','productType list','pharmaceutical_form list','route list']
#     if what == "all":
#         return data
#     elif what in what_lists:
#         i = what_lists.index(what)
#         data1 = data[dict_keys[i]]
#         return {what+" list":data1}
        
#search by grp only (req params formdata)
# @app.route("/searchbygrp", methods = ['POST'])
# def search_api_grp():
#     country = request.form.get('country')
#     print(country)
#     sourceCountry = request.form.get('sourceCountry')
#     mah_country = request.form.get('mahOrgCountry')
#     product_group = request.form.get('productGroupName')
#     productType=request.form.get('productTypeText')
#     pharmacetucal_form = request.form.get('pharmaceuticalFormText')
#     route = request.form.get('route')
#     db_name = request.form['db_name']
#     data_dict = {'country':country,'sourceCountry':sourceCountry,'mahOrgCountry':mah_country,'productGroupName':product_group,'productTypeText':productType,'pharmaceuticalFormText':pharmacetucal_form,'route':route}
#     print(data_dict)
#     user_data = {k:v for k,v in data_dict.items() if type(v)==str}
#     print(user_data,db_name)
#     final_results = joining_and_fetching_data(user_data,db_name)
#     return final_results
if __name__ == "__main__":

    app.run(host="0.0.0.0", port=8008, debug=True)