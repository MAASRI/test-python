
import psycopg2
import pandas as pd
import collections
import csv
import os
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
import json

DB_HOST = os.environ.get('postgresHost',"localhost")
DB_PASSWORD=  os.environ.get('postgresPassword',"postgres")
DB_USER = os.environ.get('postgresUser',"postgres")
DB_PORT = os.environ.get('postgresPost',"5432")


def extract_fields(user_data):
    medicinalProdId=""
    medId=""
    atcCode=""
    drugName=""
    sourceText=""
    substanceName=""
    mahName=""

    for k , v in user_data.items():
        if k == 'medicinalProdId':
            medicinalProdId = v
        elif k == 'medId':
            med = v       
        elif k == 'atcCode':
            atcCode = v.upper()
        elif k == 'drugName':
            drugName = v.capitalize()
        elif k == 'sourceText':
            sourceText = v.title()
        elif k == 'substanceName':
            substanceName = v.capitalize()
        elif k == 'mahName':
            mahName =v.capitalize()
    return medicinalProdId,medId,atcCode,drugName,sourceText,substanceName,mahName
def extract_fields_groups(user_data):
    country = ""
    sourceCountry = ""
    productGroupName = ""
    productTypeText = ""
    route = ""
    pharmaceuticalFormText = ""
    mahOrgCountry = ""
    for k , v in user_data.items():
        if k == 'country':
            country = v.title()
        elif k == 'sourceCountry':
            sourceCountry = v.title()      
        elif k == 'productGroupName':
            productGroupName = v
        elif k == 'productTypeText':
            productTypeText = v.capitalize()
        elif k == 'route':
            route = v
        elif k == 'pharmaceuticalFormText':
            if v.lower()=="unspecified":
                pharmaceuticalFormText = v.capitalize()
            else:
                pharmaceuticalFormText = v.upper()
        elif k == 'mahOrgCountry':
            mahOrgCountry =v.title()
    return country,sourceCountry,productGroupName,productTypeText,route,pharmaceuticalFormText,mahOrgCountry

def arrange_data(rows,is_only_substance,db_name):
    conn = psycopg2.connect("dbname="+db_name+" user="+DB_USER+" password="+DB_PASSWORD+" host="+DB_HOST+" port="+DB_PORT)
    conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT);
    cur = conn.cursor()
    results = []
    for row in rows:
        query = "select substanceName from joined_table where medicinalProdId ='%s'"%(row[9])
        cur.execute(query)
        sub_rows = cur.fetchall()
        sub_rows = [item  for sublist in sub_rows for item in sublist]
        sub_rows = [i for n, i in enumerate(sub_rows) if i not in sub_rows[n + 1:]]
        string = row[11]
        string = string.lstrip('0')
        string = string.rjust(4 + len(string), '0')
        query = "select * from str where strength_id ='%s'"%(string)
        cur.execute(query)
        str_rows = cur.fetchall()
        if len(str_rows)>0:
            for rw in str_rows:
                if is_only_substance == 'yes':
                    output = {"drugName":row[0],"strength":rw[1],"salesCountryName":row[1],"isGeneric":row[2],"mah":row[3],"mahCountryName":row[16],"drugCode":str(row[4])+str(row[5])+str(row[6]),"atcCode":row[7],"atcDescription":row[8],"medicinalProdId":row[9],"substanceNameList":",".join(sub_rows),"sourceCountryName":row[11],"routeOfAdministration":row[12],"productGroupName":row[13],"productType":row[14],"pharmaceuticalFormText":row[15], "medId": row[18] or ""}
                else:
                    output = {"drugName":row[0],"strength":rw[1],"salesCountryName":row[1],"isGeneric":row[2],"mah":row[3],"mahCountryName":row[16],"drugCode":str(row[4])+str(row[5])+str(row[6]),"atcCode":row[7],"atcDescription":row[8],"medicinalProdId":row[9],"substanceName":row[10],"sourceCountryName":row[11],"routeOfAdministration":row[12],"productGroupName":row[13],"productType":row[14],"pharmaceuticalFormText":row[15], "medId": row[18] or ""}
                if output not in results:
                    results.append(output)
        else:
            if is_only_substance == 'yes':
                output = {"drugName":row[0],"strength":"","salesCountryName":row[1],"isGeneric":row[2],"mah":row[3],"mahCountryName":row[16],"drugCode":str(row[4])+str(row[5])+str(row[6]),"atcCode":row[7],"atcDescription":row[8],"medicinalProdId":row[9],"substanceNameList":",".join(sub_rows),"sourceCountryName":row[11],"routeOfAdministration":row[12],"productGroupName":row[13],"productType":row[14],"pharmaceuticalFormText":row[15], "medId": row[18] or ""}
            else:
                output = {"drugName":row[0],"strength":"","salesCountryName":row[1],"isGeneric":row[2],"mah":row[3],"mahCountryName":row[16],"drugCode":str(row[4])+str(row[5])+str(row[6]),"atcCode":row[7],"atcDescription":row[8],"medicinalProdId":row[9],"substanceName":row[10],"sourceCountryName":row[11],"routeOfAdministration":row[12],"productGroupName":row[13],"productType":row[14],"pharmaceuticalFormText":row[15], "medId": row[18] or ""}
            if output not in results:
                results.append(output)       
    return results

def fetch_records_drug(drugName,db_name):
    conn = psycopg2.connect("dbname="+db_name+" user="+DB_USER+" password="+DB_PASSWORD+" host="+DB_HOST+" port="+DB_PORT)
    conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT);
    cur = conn.cursor()
    query = "select * from joined_table where drugName LIKE '%"+ '%s'%(drugName)+"%' "
    cur.execute(query)
    rows = cur.fetchall()
    results = arrange_data(rows,'no',db_name)
    query = "SELECT *, similarity(drugName, '"+'%s'%(drugName)+"')  AS sim from joined_table ORDER BY sim DESC limit 15"
    cur.execute(query)
    rows = cur.fetchall()
    results1 = arrange_data(rows,'no',db_name)
    results.extend(results1)
    return results
def fetch_records_substance(substanceName,user_data,db_name):
    conn = psycopg2.connect("dbname="+db_name+" user="+DB_USER+" password="+DB_PASSWORD+" host="+DB_HOST+" port="+DB_PORT)
    conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT);
    cur = conn.cursor()
    medicinalProdId,medId,atcCode,drugName,sourceText,substanceName,mahName = extract_fields(user_data)
    country,sourceCountry,productGroupName,productTypeText,route,pharmaceuticalFormText,mahOrgCountry = extract_fields_groups(user_data)
    query = "select * from joined_table where substanceName LIKE '%"+ '%s'%(substanceName)+"%'"
    cur.execute(query)
    rows = cur.fetchall()
    if medicinalProdId == "" and medId == "" and atcCode == "" and drugName == "" and sourceText=="" and mahName == "" and country == "" and sourceCountry == "" and productGroupName == "" and productTypeText == "" and route == "" and pharmaceuticalFormText =="" and mahOrgCountry == "":
        results = arrange_data(rows,'yes',db_name)
    else:
        results = arrange_data(rows,'no',db_name)
    query = "SELECT *, similarity(substanceName, '"+'%s'%(substanceName)+"')  AS sim from joined_table ORDER BY sim DESC limit 15"
    cur.execute(query)
    rows = cur.fetchall()
    if medicinalProdId == "" and medId == "" and atcCode == "" and drugName == "" and sourceText=="" and mahName == "" and country == "" and sourceCountry == "" and productGroupName == "" and productTypeText == "" and route == "" and pharmaceuticalFormText =="" and mahOrgCountry == "":
        results1 = arrange_data(rows,'yes',db_name)
    else:
        results1 = arrange_data(rows,'no',db_name)
    results.extend(results1) 
    return results
def query_id(colname,val,db_name):
    conn = psycopg2.connect("dbname="+db_name+" user="+DB_USER+" password="+DB_PASSWORD+" host="+DB_HOST+" port="+DB_PORT)
    conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT);
    cur = conn.cursor()
    query = f"select * from joined_table where {colname} = '%s'"%(val)
    cur.execute(query)
    rows = cur.fetchall()
    results = arrange_data(rows,'',db_name)
    return results
def query_text(colname,val,db_name):
    conn = psycopg2.connect("dbname="+db_name+" user="+DB_USER+" password="+DB_PASSWORD+" host="+DB_HOST+" port="+DB_PORT)
    conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT);
    cur = conn.cursor()
    query = f"select * from joined_table where {colname} LIKE '%"+ '%s'%(val)+"%'"
    cur.execute(query)
    rows = cur.fetchall()
    results = arrange_data(rows,'',db_name)
    query = f"SELECT *, similarity({colname}, '"+'%s'%(val)+"')  AS sim from joined_table ORDER BY sim DESC limit 15"
    cur.execute(query)
    rows = cur.fetchall()
    results1 = arrange_data(rows,'',db_name)
    results.extend(results1)
    return results

def query_grp(colname,val,db_name):
    conn = psycopg2.connect("dbname="+db_name+" user="+DB_USER+" password="+DB_PASSWORD+" host="+DB_HOST+" port="+DB_PORT)
    conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT);
    cur = conn.cursor()
    if len(val) == 9:
        query = f"select * from joined_table where {colname[0]} LIKE '%"+ '%s'%(val[0])+"%' and" + f" {colname[1]} LIKE '%"+ '%s'%(val[1])+"%' and" + f" {colname[2]} LIKE '%"+ '%s'%(val[2])+"%' and" + f" {colname[3]} LIKE '%"+ '%s'%(val[3])+"%' and" + f" {colname[4]} LIKE '%"+ '%s'%(val[4])+"%' and" + f" {colname[5]}  LIKE '%"+ '%s'%(val[5])+"%' and" + f" {colname[6]}  LIKE '%"+ '%s'%(val[6])+"%' and" + f" {colname[7]}  LIKE '%"+ '%s'%(val[7])+"%' and" + f" {colname[8]}  LIKE '%"+ '%s'%(val[8])+"%'"
    elif len(val) == 8:
        query = f"select * from joined_table where {colname[0]} LIKE '%"+ '%s'%(val[0])+"%' and" + f" {colname[1]} LIKE '%"+ '%s'%(val[1])+"%' and" + f" {colname[2]} LIKE '%"+ '%s'%(val[2])+"%' and" + f" {colname[3]} LIKE '%"+ '%s'%(val[3])+"%' and" + f" {colname[4]} LIKE '%"+ '%s'%(val[4])+"%' and" + f" {colname[5]}  LIKE '%"+ '%s'%(val[5])+"%' and" + f" {colname[6]}  LIKE '%"+ '%s'%(val[6])+"%' and" + f" {colname[7]}  LIKE '%"+ '%s'%(val[7])+"%'"
    elif len(val) == 7:
    	query = f"select * from joined_table where {colname[0]} LIKE '%"+ '%s'%(val[0])+"%' and" + f" {colname[1]} LIKE '%"+ '%s'%(val[1])+"%' and" + f" {colname[2]} LIKE '%"+ '%s'%(val[2])+"%' and" + f" {colname[3]} LIKE '%"+ '%s'%(val[3])+"%' and" + f" {colname[4]} LIKE '%"+ '%s'%(val[4])+"%' and" + f" {colname[5]}  LIKE '%"+ '%s'%(val[5])+"%' and" + f" {colname[6]}  LIKE '%"+ '%s'%(val[6])+"%'"
    elif len(val) == 6:
        query = f"select * from joined_table where {colname[0]} LIKE '%"+ '%s'%(val[0])+"%' and" + f" {colname[1]} LIKE '%"+ '%s'%(val[1])+"%' and" + f" {colname[2]} LIKE '%"+ '%s'%(val[2])+"%' and" + f" {colname[3]} LIKE '%"+ '%s'%(val[3])+"%' and" + f" {colname[4]} LIKE '%"+ '%s'%(val[4])+"%' and" + f" {colname[5]}  LIKE '%"+ '%s'%(val[5])+"%'"
    elif len(val) == 5:
        query = f"select * from joined_table where {colname[0]} LIKE '%"+ '%s'%(val[0])+"%' and" + f" {colname[1]} LIKE '%"+ '%s'%(val[1])+"%' and" + f" {colname[2]} LIKE '%"+ '%s'%(val[2])+"%' and" + f" {colname[3]} LIKE '%"+ '%s'%(val[3])+"%' and" + f" {colname[4]} LIKE '%"+ '%s'%(val[4])+"%'"
    elif len(val) == 4:
        query = f"select * from joined_table where {colname[0]} LIKE '%"+ '%s'%(val[0])+"%' and" + f" {colname[1]} LIKE '%"+ '%s'%(val[1])+"%' and" + f" { colname[2]} LIKE '%"+ '%s'%(val[2])+"%' and" + f" {colname[3]} LIKE '%"+ '%s'%(val[3])+"%'"
    elif len(val) == 3:
        query = f"select * from joined_table where {colname[0]} LIKE '%"+ '%s'%(val[0])+"%' and" + f" {colname[1]} LIKE '%"+ '%s'%(val[1])+"%' and" + f" {colname[2]} LIKE '%"+ '%s'%(val[2])+"%'"
    elif len(val) == 2:
        query = f"select * from joined_table where {colname[0]} LIKE '%"+ '%s'%(val[0])+"%' and" + f" {colname[1]} LIKE '%"+ '%s'%(val[1])+"%'"
    elif len(val) ==1:
        query = f"select * from joined_table where {colname[0]} LIKE '%"+ '%s'%(val[0])+"%' "
    # print(query)
    cur.execute(query)
    rows = cur.fetchall()
    results = arrange_data(rows,'no',db_name)
    return results
def joining_table(db_name):
    try:
        conn = psycopg2.connect("dbname="+db_name+" user="+DB_USER+" password="+DB_PASSWORD+" host="+DB_HOST+" port="+DB_PORT)
        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT);
        cur = conn.cursor()
    except (Exception, psycopg2.DatabaseError) as error:
        return {"message": "Database not exists hit api -- createdb "}
    cmd = "DROP TABLE IF EXISTS joined_table"
    cur.execute(cmd)
    cmd = "DROP TABLE IF EXISTS srce_ext"
    cur.execute(cmd)
    cmd = "DROP TABLE IF EXISTS org_ext"
    cur.execute(cmd)
#joining table
    query = "create table srce_ext as  select distinct srce.source_code,srce.source,srce.country_code,ccode.country_name as srce_country_name from srce inner join ccode on srce.country_code = ccode.country_code"
    cur.execute(query)
    query = "create table org_ext as  select distinct org.organization_id,org.name as mahName,org.country_code,ccode.country_name as mah_country_name from org  inner join ccode on org.country_code = ccode.country_code"
    cur.execute(query)
    query = "create table joined_table as select distinct  mp.drugName,ccode.country_name,mp.generic,org_ext.mahName,mp.drug_record_number,mp.sequence_number_1,mp.sequence_number_2,atc.atcCode,atc.atc_text,mp.medicinalProdId,sun.substanceName,srce_ext.srce_country_name,pp.route_of_administration,prg.productGroupName,prt.prt_text,pf.pf_text,org_ext.mah_country_name,mp.sequence_number_4,mp.medid,mp.country,mp.source_code,mp.sequence_number_3,  org_ext.country_code,srce_ext.source from mp inner join prg on prg.productgroup_id = mp.product_group inner join prt on prt.prodtype_id=mp.productType inner join org_ext on org_ext.organization_id = mp.marketing_authorization_holder inner join pp on pp.medicinalProdId = mp.medicinalProdId inner join pf on pf.pharmform_id = pp.pharmaceutical_form inner join thg on thg.medicinalProdId = mp.medicinalProdId  inner join atc on atc.atcCode = thg.atccode inner join ing on ing.medicinalProdId = mp.medicinalProdId inner join sun on sun.substance_id = ing.substance_id inner join srce_ext on srce_ext.source_code = mp.source_code inner join ccode on ccode.country_code =mp.country"
    cur.execute(query)
    conn = psycopg2.connect("dbname="+db_name+" user="+DB_USER+" password="+DB_PASSWORD+" host="+DB_HOST+" port="+DB_PORT)
    conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT);
    cur = conn.cursor()
    # query = "select * from joined_table"
    # cur.execute(query)
    # df = pd. DataFrame(cur.fetchall())
    # df.to_csv("tables_innerjoin_results.csv")
    # colnames = [desc[0] for desc in cur.description]
    # print(colnames)
#creating extension pg_trgm
    cmd = "DROP extension IF EXISTS pg_trgm,btree_gist"
    cur.execute(cmd)
    query = "create extension pg_trgm with schema pg_catalog;"
    cur.execute(query)
    query = "CREATE EXTENSION btree_gist"
    cur.execute(query)
    query = "CREATE INDEX trgm_idx ON joined_table USING gist (drugName,substanceName,source,mahName  gist_trgm_ops)"
    cur.execute(query)
    conn.commit()
def joining_and_fetching_data(user_data,db_name):
    joining_table(db_name)
    
    try:
        conn = psycopg2.connect("dbname="+db_name+" user="+DB_USER+" password="+DB_PASSWORD+" host="+DB_HOST+" port="+DB_PORT)
        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT);
        cur = conn.cursor()
    except (Exception, psycopg2.DatabaseError) as error:
        return {"message": "Database not exists hit api -- createdb "}


#getting values..
    medicinalProdId,medId,atcCode,drugName,sourceText,substanceName,mahName = extract_fields(user_data)
    names = [country,sourceCountry,productGroupName,productTypeText,route,pharmaceuticalFormText,mahOrgCountry] = extract_fields_groups(user_data)
    names_final = [e for e in names if e!=""]
    final_results = []
    names_text = [drugName,sourceText,substanceName,mahName]
    text_names = [e for e in names_text if e!=""]
    names_id = [medicinalProdId,medId,atcCode]
    id_names  = [e for e in names_id if e!=""]
    if len(names_final)>0 or (len(id_names)>0 and len(text_names)>0):

	#search by group & text & id implementation
	    names = [country,sourceCountry,productGroupName,productTypeText,route,pharmaceuticalFormText,mahOrgCountry]
	    names1 = [medicinalProdId,medId,atcCode,drugName,sourceText,substanceName,mahName]
	    names.extend(names1)
	    colnames_list = ['country_name','srce_country_name','productGroupName','prt_text','route_of_administration','pf_text','mah_country_name','medicinalProdId','medid','atcCode','drugName','source','substanceName','mahName']
	    val = [i  for i in names if i]

	    colname = [ colnames_list[names.index(i)] for i in names if i]
	    # print(val,'values....',colname)
	    if len(val) > 0:
	        results = query_grp(colname,val,db_name)
	        final_results.extend(results)
    else:
	#only search by id
	    if medicinalProdId!="":
	        results = query_id('medicinalProdId',medicinalProdId,db_name)
	        final_results.extend(results) 
	    if medId!="":
	        results = query_id('medid',medId,db_name)
	        final_results.extend(results) 
	    if atcCode!="":
	        results = query_id('atcCode',atcCode,db_name)
	        final_results.extend(results) 

	#only search by text

	    if drugName!="" :
	        results = fetch_records_drug(drugName,db_name)
	        final_results.extend(results)
	    if substanceName!="":
	        results = fetch_records_substance(substanceName,user_data,db_name)
	        final_results.extend(results) 
	    if sourceText!="":
	        results = query_text('source',sourceText,db_name)
	        final_results.extend(results) 
	    if mahName !="":
	        results = query_text('mahName',mahName,db_name)
	        final_results.extend(results) 

    final_results = [i for n, i in enumerate(final_results) if i not in final_results[n + 1:]]
    return {"data":final_results}
def show_dropdown_data(db_name):
    try:
        conn = psycopg2.connect("dbname="+db_name+" user="+DB_USER+" password="+DB_PASSWORD+" host="+DB_HOST+" port="+DB_PORT)
        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT);
        cur = conn.cursor()
    except (Exception, psycopg2.DatabaseError) as error:
        return {"message": "Database not exists hit api -- createdb ", "statusCode": 400}
    joining_table(db_name)
    query = "select country_name,srce_country_name,productGroupName,prt_text,pf_text,mah_country_name,route_of_administration from joined_table"
    cur.execute(query)
    rows =  cur.fetchall()
    country_names = []
    sourceCountry = []
    product_grp = []
    productType = []
    pf = []
    mah_country = []
    route_of_administrations =[]
    for row in rows:
        country_names.append({"label":row[0],"value":row[0]})
        sourceCountry.append({"label":row[1],"value":row[1]})
        product_grp.append({"label":row[2],"value":row[2]})
        productType.append({"label":row[3],"value":row[3]})
        pf.append({"label":row[4],"value":row[4]})
        mah_country.append({"label":row[5],"value":row[5]})
        route_of_administrations.append({"label":row[6],"value":row[6]})
    country_names = [i for n, i in enumerate(country_names) if i not in country_names[n + 1:]]
    sourceCountry = [i for n, i in enumerate(sourceCountry) if i not in sourceCountry[n + 1:]]
    product_grp = [i for n, i in enumerate(product_grp) if i not in product_grp[n + 1:]]
    productType = [i for n, i in enumerate(productType) if i not in productType[n + 1:]]
    pf = [i for n, i in enumerate(pf) if i not in pf[n + 1:]]
    mah_country = [i for n, i in enumerate(mah_country) if i not in mah_country[n + 1:]]
    route_of_administrations = [i for n, i in enumerate(route_of_administrations) if i not in route_of_administrations[n + 1:]]
    return country_names,sourceCountry,product_grp,productType,pf,mah_country,route_of_administrations

