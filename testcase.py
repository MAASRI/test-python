import pytest
from app1 import app
import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

@pytest.fixture
def client():
	app.config['TESTING'] = True
	app.config['WTF_CSRF_ENABLED'] = False
	app.config['DEBUG'] = True
	app.config['THREADED'] = True

	with app.test_client() as client:
		yield client

@pytest.fixture(scope='module')
def tempdb(request):
	conn = psycopg2.connect(user="postgres", host="localhost" ,port=5432)
	cur = conn.cursor()
	yield cur
	cur.close()

@pytest.fixture(scope='module')
def tempdbwhodd(request):
	conn = psycopg2.connect(user="postgres", host="localhost" ,port=5432)
	conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
	db_name = 'clientId'
	cur = conn.cursor()
	command = f"SELECT * FROM pg_stat_activity WHERE datname = '{db_name}';"
	cur.execute(command)
	command = f"SELECT	pg_terminate_backend (pid) FROM	pg_stat_activity WHERE	pg_stat_activity.datname = '{db_name}';"
	cur.execute(command)
	command = "DROP DATABASE IF EXISTS \"" + db_name + "\";"
	cur.execute(command)
	command = "CREATE DATABASE \"" + db_name + "\";"
	cur.execute(command)
	cur.close()

# Meddra
def test1_positive(tempdb,client):
	response = client.post('/createclientdb', json={
   "clientId":"1001",
   "paths":{
	 "meddra_release":"https://azure-production.s3.amazonaws.com/meddra_release.asc",
	 "soc_hlgt":"https://azure-production.s3.amazonaws.com/soc_hlgt.asc",
	 "soc":"https://azure-production.s3.amazonaws.com/soc.asc",
	 "smq_list":"https://azure-production.s3.amazonaws.com/smq_list.asc",
	 "smq_content":"https://azure-production.s3.amazonaws.com/smq_content.asc",
	 "pt":"https://azure-production.s3.amazonaws.com/pt.asc",
	 "mdhier":"https://azure-production.s3.amazonaws.com/mdhier.asc",
	 "llt":"https://azure-production.s3.amazonaws.com/llt.asc",
	 "intl_ord":"https://azure-production.s3.amazonaws.com/intl_ord.asc",
	 "hlt_pt":"https://azure-production.s3.amazonaws.com/hlt_pt.asc",
	 "hlt":"https://azure-production.s3.amazonaws.com/hlt.asc",
	 "hlgt_hlt":"https://azure-production.s3.amazonaws.com/hlgt_hlt.asc",
	 "hlgt":"https://azure-production.s3.amazonaws.com/hlgt.asc"
 }
})

	assert response.status_code == 200


def test1_negative(tempdb,client):
	response = client.post('/createclientdb', json= {})
	assert response.status_code == 500

def test2_positive(tempdb,client):
	response = client.post('/get_versions', json= {"clientId": "1001"})
	assert response.status_code == 200

def test2_negative(tempdb,client):
	response = client.post('/get_versions', json= {})
	assert response.status_code == 500

def test3_positive_searchall_ext(tempdb,client):
	response = client.post('/searchall_extension', json= {"clientId": "1001",
 	"search_term" : "11-beta-hydroxylase deficiency", "version" : "23.0"})
	assert response.status_code == 200

def test3_positive_searchall_extllt(tempdb,client):
	response = client.post('/searchall_extension', json= {"clientId": "1001",
  "search_term" : "11-beta-hydroxylase deficiency","table":"llt",
  "version" : "23.0"})
	assert response.status_code == 200

def test3_positive(tempdb,client):
	response = client.post('/searchall', json= {"clientId": "1001",
	"search_term" : "17 ketosteroids urine decreased",
	"version" : "23.0"})
	assert response.status_code == 200

def test3_positive_llt(tempdb,client):
	response = client.post('/searchall', json= {"clientId": "1001",
	"search_term" : "11-beta-hydroxylase deficiency",
	"table" : "llt",
	"version" : "23.0"})
	assert response.status_code == 200

def test4_positive(tempdb,client):
#search in lower order table
	response = client.post('/searchlevel', json= {"clientId": "1001",
	"search_id" : "10000032","table": "hlt","version" : "23.0"})
	assert response.status_code == 200

def test5_positive(tempdb,client):
#search in pt
	response = client.post('/searchlevel', json= {"clientId": "1001",
	"search_id" : "10000044","table": "pt","version" : "23.0"
})
	assert response.status_code == 200

def test4_negative(tempdb,client):
	response = client.post('/searchlevel', json= {"clientId": "1000",
	"search_id" : "10000032","table": "hlt","version" : "23.0"})
	assert response.status_code == 500

def test5_negative(tempdb,client):
	response = client.post('/searchlevel', json= {"clientId": "1001",
"search_id" : "10000032","table": "llt","version" : "23.0"
})
	assert response.status_code == 404

def test7_positive(tempdb,client):
	response = client.post('/search_individual', json= {"clientId": "1001",
	"search_term" : "Cardiac conduction disorders","table": "hlt","version" : "23.0"})
	assert response.status_code == 200

def test6_negative(tempdb,client):
	response = client.post('/search_individual', json= {"clientId": "1000",
	"search_term" : "Cardiac conduction disorders","table": "hlt","version" : "23.0"
})
	assert response.status_code == 500

def test7_negative(tempdb,client):
	response = client.post('/search_individual', json= {"clientId": "1001",
	"search_term" : "CCD","table": "hlt","version" : "23.0"
})
	assert response.status_code == 404

def test20_positive(tempdb,client):
	response = client.post('/read', json= {"clientId": "1001",
	"table": "hlt","version" : "23.0"
})
	assert response.status_code == 200

def test20_negative(tempdb,client):
	response = client.post('/read', json= {"clientId": "1001", "table": "None",
	"version" : "23.0"
})
	assert response.status_code == 500


#WhoDD
def test8_positive(tempdbwhodd,client):
	response = client.post('/c_format/createdb', json= {"folder_name":"./WhoDD", "clientId":"tempwhodd"})
	assert response.status_code == 200

def test9_positive(tempdbwhodd,client):
	response = client.post('/c_format/search', json= {"medicinalProdId": 105,
"clientId":"tempwhodd"
})
	assert response.status_code == 200

def test10_positive(tempdbwhodd,client):
	response = client.post('/c_format/search', json= {"mahName": "Warner-lambert consumer healthcare",
"clientId":"tempwhodd"
})
	assert response.status_code == 200

def test9_negative(tempdbwhodd,client):
	response = client.post('/c_format/search', json= {"mahName": "Warner-lambert consumer healthcare"})
	assert response.status_code == 400

def test10_negative(tempdbwhodd,client):
	response = client.post('/c_format/search', json= {"clientId":"tempwhodd"})
	assert response.status_code == 400

def test11_negative(tempdbwhodd,client):
	response = client.post('/c_format/search', json= {"medicinalProdId": 105144,
	"medId": "null","clientId":"tempwhodd"})
	assert response.status_code == 400

def test12_negative(tempdbwhodd,client):
	response = client.post('/c_format/search', json= {"drugName": "Panadol",
	"mahName": "Warner-lambert consumer healthcare","clientId":"tempwhodd"})
	assert response.status_code == 400

def test13_positive(tempdbwhodd,client):
	response = client.post('/c_format/search', json= {"drugName": "Atasol",
	"clientId":"tempwhodd"})
	assert response.status_code == 200

def test14_positive(tempdbwhodd,client):
	response = client.post('/c_format/list', json= {"clientId":"tempwhodd",
"what" : "all"})
	assert response.status_code == 200

def test15_positive(tempdbwhodd,client):
	response = client.post('/c_format/list', json= {"clientId":"tempwhodd",
"what" : "pharmacetucalForm"})
	assert response.status_code == 200

def test14_negative(tempdbwhodd,client):
	response = client.post('/c_format/list', json= {"what" : "pharmacetucalForm"})
	assert response.status_code == 400

def test15_negative(tempdbwhodd,client):
	response = client.post('/c_format/list', json= {"clientId":"tempwhodd",
"what" : 123})
	assert response.status_code == 400


