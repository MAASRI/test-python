*Modifications in app1.py*:
Changed reading ascii file in createDB() &  db_initialiser(paths, db) functions to read local paths instead of urllib request just used open.

Observe that if searched 'reye's syndrome' getting syntax error so added code to remove " ' " in searched term  & while comparing with llt terms and pt terms also removing " ' "

function new_all_search_api_extension() :
Pseudocode Logic: 
'app1.py':
    • Connect to database 
    • fetch all the terms like, similarity  terms related to llt_search term

  pass the resultent terms along with search term..db_name, etc.. to terms_search() function by improting python file(terms_search_ordering.py)

'terms_search_ordering.py':
    • AFTER getting results make a seperate lists of  ‘llt_names’ and ‘pt_names’  

    • check if pt_name ‘ does not exists’ in llt_names. Search the pt_term to fetch all the related terms 

    • Make a final list of resultent terms without duplicates...

**similarity _score(field_name)**:
    • Break down the search_term to Word bigram and character trigram
    • if word bigram length not equal to zero (for multiple word search)
	    • If search term equals to field value count becomes 1 else sum of cosine similariy of each bigram word with field value

		Ex1:
		    • search term -- > RED EYE ---> ['red eye'] or ['eye red']
		    • llt name --->  --->eye red 
		    • count of matched bigrams = 1
		    • similarity _score for word bigrams  = 1(matched with bigrams)/1(actual no.of bigrams) == 1.

		Ex2:
		    • search term -- > blood acid phosphatase ---> word bigrams --->  blood acid , acid phosphatase
		    • llt name ---> acid phosphatase increased 
		    • count of matched bigrams =cosine similarity(blood acid,acid phosphatase increased)--->0.40824829046386296+
			cosine similarity(acid phosphatase,acid phosphatase increased)-->0.8164965809277259 =  1.224744871
		    • similarity _score for word bigrams  = 1.224744871(matched with bigrams)/2(actual no.of bigrams) == 0.612372436

   	    • similarity _score for word bigrams = count of bigrams matches / actual no.of bigrams		
	    • count the no.of character trigrams matches in field_name
	    • similarity of character trigram = count of character trigrams matches in field value/actualno.ofcharacter trigrams of search term
	    • similarity score = 80%similarity of word bigrams matches + 20% similarity of character trigrams.
    • Else (for single word search):
	    • word_similarity_score = Cosine similarity of search term and field value
	    • count the no.of character trigrams matches in field_name
	    • similarity of character trigram = count of character trigrams matches in field value/actualno.of character trigrams of searchterm
	    • similarity score = similarity of character trigram
	 similarity score = 80% word similarity  + 20% similarity of character trigrams.
NOTE: converting search term and field values ( llt names or pt names ) to lemmas
NOTE: Word bigrams will modify if '(' in search term
# ex: Rhesus antibodies (anti-D) --> word_bi_gram_result = ['rhesus antibody', 'antibody (', '( anti-d', 'anti-d )'] to make it correct -->['rhesus antibody', 'antibody (anti-d)']' 

1) field_name = llt_name 
2) field_name = pt_name

Composite score:
if search term llt then 80% of llt_similarityscore+20% of pt similarity score
else:
average of llt_similarityscore and pt similarity score.

Cosine similarity formula:
    sum of product of count of common words  from each string /product of  sqrt of sum of square of individual words count ofstrings 
   ex:
    • red eye , --> red - 1, eye -1
    • eye red eye paining ---> eye - 2, red -1 , paining -1
    • common words --> red, eye
    • numerator = (1*1) + (1*2) 
    • denominator = sqrt (1^2 + 1^2) * sqrt(2^2+1^2+1^2) 
