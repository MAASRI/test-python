from math import log
import pandas as pd
import os
import re

from sqlalchemy import create_engine
import psycopg2
import psycopg2.extras
import io
import urllib.request
import copy
import json
import uuid
import time
import requests
from pathlib import Path
from datetime import datetime
import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
from flask import Flask, request
from flask_cors import CORS
from urllib.request import urlretrieve
from test_app1 import term_search
from new_search import new_term_search
import ssl

from app1_C_fomat import *
from terms_search_ordering import terms_search
from terms_ordering import terms_search_whodd
from rr_python import rr_python as rr
import uuid
ssl._create_default_https_context = ssl._create_unverified_context

DB_HOST = os.environ.get('postgresHost', "localhost")
DB_PASSWORD = os.environ.get('postgresPassword', "postgres")
DB_USER = os.environ.get('postgresUser', "postgres")
DB_PORT = os.environ.get('postgresPost', "5432")

rr.setupLogHandler("MedDRA", "DEBUG")
app = Flask(__name__)
CORS(app)

tables = {
	"llt": {
		"above": "pt",
		"below": None,
		"id_var": "llt_code"
	},
	"pt": {
		"above": "hlt_pt",
		"below": None,
		"id_var": "pt_code"
	},
	"hlt": {
		"above": "hlgt_hlt",
		"below": "hlt_pt",
		"id_var": "hlt_code"
	},
	"hlgt": {
		"above": "soc_hlgt",
		"below": "hlgt_hlt",
		"id_var": "hlgt_code"
	},
	"soc": {
		"above": None,
		"below": "soc_hlgt",
		"id_var": "soc_code"
	},
}
cols = {

	"hlgt": ['hlgt_code', 'hlgt_name'],

	"hlgt_hlt": ['hlgt_code', 'hlt_code'],

	"hlt": ['hlt_code', 'hlt_name'],

	"hlt_pt": ['hlt_code', 'pt_code'],

	"intl_ord": ['intl_ord_code', 'soc_code'],

	"llt": ['llt_code', 'llt_name', 'pt_code', 'llt_currency'],

	"meddra_release": ["version", "language"],

	"mdhier": ["pt_code", "hlt_code", "hlgt_code", "soc_code",
			   "pt_name",
			   "hlt_name",
			   "hlgt_name",
			   "soc_name",
			   "soc_abbrev",
			   "pt_soc_code",
			   "primary_soc_fg", ],

	"meddra_history_english": ["term_code",
							   "term_name",
							   "term_addition_version",
							   "term_type",
							   "llt_currency",
							   "action"],

	"pt": ['pt_code', 'pt_name', 'pt_soc_code'],

	"smq_content": ["smq_code",
					"term_code",
					"term_level",
					"term_scope",
					"term_category",
					"term_weight",
					"term_status",
					"term_addition_version",
					"term_last_modified_version"],

	"smq_list": ["smq_code",
				 "smq_name",
				 "smq_level",
				 "smq_description",
				 "smq_source",
				 "smq_note",
				 "MedDRA_version",
				 "status",
				 "smq_algorithm"],

	"soc": ['soc_code', 'soc_name', 'soc_abbrev'],

	"soc_hlgt": ['soc_code', 'hlgt_code'],
}

def list_for_query(ar):
	ar=str(ar)
	ar=ar[1:-1]
	ar = "("+ar+")"
	return ar

def db_initialiser(paths, db):
	for file_name, path in paths.items():
		# file_path=""
		# f=open(file_path)
		# data = f.read()
		# f.close()
		print("in db initialiser", flush=True)
		rr.rrAdapter("INFO", "In DB initialiser", logDict)
		with urllib.request.urlopen(path) as file_url:
		   data = file_url.read()
# *********changed.............to read local paths........******
		#with open(path, 'r') as file_url:
		#	data = file_url.read()
		print("file read finished for ", file_name, flush=True)
		rr.rrAdapter("INFO", "Performing file read", logDict)
		data = data.decode("ASCII")
		data = re.sub("\$+", "@", data)
		data = data.strip()

		data = data.split("\n")

		main_li = []
		for d in data:
			d = d.strip()
			d = d.split("@")

			if d[-1] is "":
				d.remove("")

			main_li.append(d)
		# nm=file[:-4]
		nm = file_name

		# add condition to check if all columns present
		df = eval("pd.DataFrame(main_li, columns = " + str(cols[nm]) + ")")
		ans = postgres_insert(nm, df, db)
		if not ans:
			rr.rrAdapter("ERROR", "Failed to create client DB 1", logDict)
			return {"statusCode": 500, "message": "Failed to create Client DB 1"}
			break
	return


def postgres_insert(nm, df, db):
	try:
		engine = create_engine('postgresql://'+DB_USER+':' +
							   DB_PASSWORD+'@'+DB_HOST+':'+DB_PORT+'/' + db)
		conn = psycopg2.connect("dbname=" + db + " user="+DB_USER +
								" password="+DB_PASSWORD+" host="+DB_HOST+" port="+DB_PORT)

		cur = conn.cursor()
	except (Exception, psycopg2.DatabaseError) as error:
		return (False)

	# truncates the table
	eval("df.head(0).to_sql(\"" + nm + "\", engine, if_exists='replace',index=False)")

	#     conn = engine.raw_connection()

	output = io.StringIO()
	df.to_csv(output, sep='\t', header=False, index=False)
	output.seek(0)
	contents = output.getvalue()
	cur.copy_from(output, nm, null="")  # null values become ''
	conn.commit()
	cur.close()
	cur.close()
	return True


# conn = psycopg2.connect("user=postgres password=postgres host=db port=5432")

# fileDataDb = client["files_data"]
# form_extraction_collection = fileDataDb["form_extraction"]

# db = client.pvciCaseExtractorAuditDB
# request_log = db.caseRequestLog
# process_log = db.caseProcessLog
# transaction_log = db.caseTransactionLog

@app.route("/createclientdb", methods=['POST'])
def createDB():
	global logDict
	try:
		body = json.loads(request.data)
		db_name = body["clientId"]
		clientId = body["clientId"]
		paths = body["paths"]
		Id = request.headers.get("rrId")
		if Id is None:
			Id = str(uuid.uuid4())
		head = {"rrId": Id}
		logDict = {"rrId": Id, "source": "Medictionary-service"}
		rr.rrAdapter("INFO", "Request Received for creating client DB", logDict)
		conn = None
	except Exception as e:
		rr.rrAdapter("ERROR", "Error occured while receiving the request for creating client DB", logDict)
		return {"statusCode": 500, "message": str(e), "data": str(e)}, 500
	try:
		rr.rrAdapter("INFO", "Trying to connect to client DB", logDict)
		print("Trying to connect", flush=True)
		conn = psycopg2.connect("user="+DB_USER+" password=" +
								DB_PASSWORD+" host="+DB_HOST+" port="+DB_PORT)

		conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
		cur = conn.cursor()
		rr.rrAdapter("INFO", "Successfully connected to client DB", logDict)
		# access meddra_release file here and get version number
		with urllib.request.urlopen(paths["meddra_release"]) as file_url:
			data = file_url.read()
		data = data.decode("ASCII")
# *********changed.............to read local paths........*******
		# with open(paths["meddra_release"],'r') as file_url:
		#     data = file_url.read()
		data = re.sub("\$+", "@", data)
		data = data.strip()

		data = data.split("\n")

		main_li = []
		for d in data:
			d = d.strip()
			d = d.split("@")

			if d[-1] is "":
				d.remove("")

			main_li.append(d)
		# nm=file[:-4]
		version_num = main_li[0][0]
		db_name = db_name+"__"+version_num
		add_to_versions(clientId, version_num)
		command = "CREATE DATABASE \"" + db_name + "\";"
		print(command, flush=True)
		cur.execute(command)
		rr.rrAdapter("INFO", "Successfully created client DB", logDict)
		# close communication with the PostgreSQL database server

	except (Exception, psycopg2.DatabaseError) as error:
		rr.rrAdapter("INFO", "Deleting the older DB", logDict)
		print(error, flush=True)
		print("here deleting older db", flush=True)
		if error.pgcode == "42P04":
			command = f"SELECT * FROM pg_stat_activity WHERE datname = '{db_name}';"
			cur.execute(command)
			command = f"SELECT	pg_terminate_backend (pid) FROM	pg_stat_activity WHERE	pg_stat_activity.datname = '{db_name}';"
			cur.execute(command)
			command = "DROP DATABASE IF EXISTS \"" + db_name + "\";"
			cur.execute(command)
			command = "CREATE DATABASE \"" + db_name + "\";"
			cur.execute(command)
			rr.rrAdapter("INFO","Creating a new client DB",logDict)
		else:
			rr.rrAdapter("ERROR","Failed to create Client DB",logDict)
			return {"statusCode": 500, "message": "Failed to create Client DB 2223"}

	# except (Exception, psycopg2.DatabaseError) as error:
	#     print(error,flush=True)
	#     print("here",flush=True)
	#     return {"statusCode": 500, "message": "Failed to create Client DB 2"}, 500
	#rr.rrAdapter("INFO","Started DB initialisation",logDict)
	print("db initialisation started",flush=True)
	db_initialiser(paths, db_name)
	conn = psycopg2.connect("dbname=" + db_name + " user="+DB_USER+" password="+DB_PASSWORD+" host="+DB_HOST+" port="+DB_PORT)
	cur = conn.cursor()
	rr.rrAdapter("INFO", "MedDRA DB initialized for user",logDict)
	# postgres_logger(clientId, "MedDRA db initialized for user")  #
	command = "create table index_1 (table_name VARCHAR(10), term_name VARCHAR(255), term_code VARCHAR(255));"
	cur.execute(command)
	command = "insert into index_1 (select 'hlgt' as table_name, hlgt_name as term_name, hlgt_code as term_code from hlgt);"
	cur.execute(command)
	command = "insert into index_1 (select 'hlt' as table_name, hlt_name as term_name, hlt_code as term_code from hlt);"
	cur.execute(command)
	command = "insert into index_1 (select 'pt' as table_name, pt_name as term_name, pt_code as term_code from pt);"
	cur.execute(command)
	command = "insert into index_1 (select 'llt' as table_name, llt_name as term_name, llt_code as term_code from llt);"
	cur.execute(command)
	command = "insert into index_1 (select 'soc' as table_name, soc_name as term_name, soc_code as term_code from soc);"
	cur.execute(command)
	command = "CREATE EXTENSION pg_trgm;"
	cur.execute(command)
	print("db initialisation done",flush=True)
	cur.close()
		# commit the changes
	conn.commit()
	if conn is not None:
		cur.close()
	rr.rrAdapter("INFO", "Successfully created and initialised Client DB",logDict)
	return {"statusCode": 200, "message": "Successfully created and initialised Client DB"}, 200

def add_to_versions(clientId, version):
	try:
		conn = psycopg2.connect("dbname= 'meddra_client_versions' user="+DB_USER+" password="+DB_PASSWORD+" host="+DB_HOST+" port="+DB_PORT)
		conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
		cur = conn.cursor()
		rr.rrAdapter("INFO", "Successfully connected to meddrs_client_versions DB", logDict)
	except Exception as err:
		conn = psycopg2.connect("user="+DB_USER+" password="+DB_PASSWORD+" host="+DB_HOST+" port="+DB_PORT)
		cur = conn.cursor()
		conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
		command = "create DATABASE meddra_client_versions;"
		cur.execute(command)
		rr.rrAdapter("INFO", "Successfully created meddrs_client_versions DB", logDict)
	try:
		command = f"insert into meddra_version values('{clientId}','{version}');"
		cur.execute(command)
		rr.rrAdapter("INFO", "Successfully inserted values into meddra_version", logDict)
	except Exception as err:
		command = "create table meddra_version (clientId VARCHAR(50), version VARCHAR(55));"
		cur.execute(command)
		command = "CREATE INDEX client_version ON meddra_version (clientId, version);"
		cur.execute(command)
		command = f"insert into meddra_version values('{clientId}','{version}');"
		cur.execute(command)
		rr.rrAdapter("INFO", "Successfully created table and inserted values into meddra_version", logDict)
	print("end?")


@app.route("/read", methods=['POST'])
def readDb():
	try:
		body = json.loads(request.data)
		clientId = body["clientId"]
		table = body['table']
		version = body['version']
		db_name = clientId+"__"+version
		Id = request.headers.get("rrId")
		if Id is None:
			Id = str(uuid.uuid4())
		head = {"rrId":Id}
		logDict = {"rrId":Id,"source":"Medictionary-service"}
		rr.rrAdapter("INFO","Request Received for reading the client DB",logDict)

		if table is None:
			rr.rrAdapter("INFO","No records found in DB",logDict)
			print("hello none",flush=True)

		if table is "":
			print("hello",flush=True)

		connection = psycopg2.connect(user=DB_USER,
									  password=DB_PASSWORD,
									  host=DB_HOST,
									  port=DB_PORT,
									  database=db_name)
		cursor = connection.cursor()
		postgreSQL_select_Query = "select * from "+table

		cursor.execute(postgreSQL_select_Query)
		table_records = cursor.fetchall()
		rr.rrAdapter("INFO","Data fetched successfully",logDict)
		return {"statusCode": 200, "message": "Success","data":table_records}, 200
	except (Exception, psycopg2.Error) as error:
		print("Error while fetching data from PostgreSQL", error,flush=True)
		rr.rrAdapter("ERROR","Error while fetching data from PostgreSQL",logDict)
		return {"statusCode": 500, "message": "error "+str(error)}, 500

	# finally:
	#     # closing database connection.
	#     if (connection):
	#         cursor.close()
	#         connection.close()
	#         print("PostgreSQL connection is closed",flush=True)

@app.route("/get_versions", methods=['POST'])
def getVersions():
	global logDict
	try:
		body = json.loads(request.data)
		clientId = body["clientId"]
		print("INSODE")
		Id = request.headers.get("rrId")
		if Id is None:
			Id = str(uuid.uuid4())
		head = {"rrId": Id}
		logDict = {"rrId": Id, "source": "Medictionary-service"}
		rr.rrAdapter("INFO", "Request Received for fetching meddra_client_versions", logDict)
		connection = psycopg2.connect(database='meddra_client_versions',
									  user=DB_USER,
									  password=DB_PASSWORD,
									  host=DB_HOST,
									  port=DB_PORT)
		cursor = connection.cursor()
		cursor = connection.cursor(cursor_factory = psycopg2.extras.DictCursor)
		postgreSQL_select_Query = f"select * from meddra_version WHERE meddra_version.clientId = '{clientId}' ;"
		# postgreSQL_select_Query= f"select * from meddra_version"

		cursor.execute(postgreSQL_select_Query)
		table_records = cursor.fetchall()
		print(table_records)
		table_records = [i["version"] for i in table_records]
		rr.rrAdapter("INFO", "Successfully fetched meddra_client_versions", logDict)
		return {"statusCode": 200, "message": "Success","versions":table_records}, 200

	except (Exception, psycopg2.Error) as error:
		print("Error while fetching data from PostgreSQL", error,flush=True)
		rr.rrAdapter("ERROR", "Error while fetching meddra_client_versions from PostgreSQL", logDict)
		return {"statusCode": 500, "message": "error "+str(error)}, 500


# ***********************************previous search all ***********************
@app.route("/searchall_extension", methods = ['POST'])
def new_all_search_api():
	body = json.loads(request.data)
	db_name = body["clientId"]
	clientId = body["clientId"]
	search_term= body["search_term"]
	version = body["version"]
	Id = request.headers.get("rrId")
	if Id is None:
		Id = str(uuid.uuid4())
	head = {"rrId":Id}
	logDict = {"rrId":Id,"source":"medictionary-service"}
	rr.rrAdapter("INFO","Request received for searching the extensions",logDict)
	db_name = db_name+"__"+version
	lim = body.get("limit", 15)
	if ("table" in list(body.keys())):
		table = body["table"]
		a = new_term_search(db_name, search_term, lim, table, traverse=False)
		rr.rrAdapter("INFO","Performing search in the extensions",logDict)
	else :
		table = "llt"
		a = new_term_search(db_name, search_term, lim, "llt")
		rr.rrAdapter("INFO","Performing search in llt table",logDict)
	# saving output response.....
	# with open('%s_searchall_before_%s.txt'%(search_term,len(a['data'])), 'w') as outfile:
	#     json.dump(a, outfile,indent=4)
	rr.rrAdapter("INFO","Performed search successfully",logDict)
	return a

# **********extended search all api... **************************************************
@app.route("/searchall", methods = ['POST'])
def new_all_search_api_extension():
	body = json.loads(request.data)
	db_name = body["clientId"]
	clientId = body["clientId"]
	search_term= body["search_term"]
	version = body["version"]
	Id = request.headers.get("rrId")
	if Id is None:
		Id = str(uuid.uuid4())
	head = {"rrId":Id}
	logDict = {"rrId":Id,"source":"medictionary-service"}
	rr.rrAdapter("INFO","Request received for searching the extensions",logDict)
	db_name = db_name+"__"+version
	lim = body.get("limit", 15)
# added****
	if "'" in search_term:
		search_term = search_term.replace("'", '')
	if ("table" in list(body.keys())):
		table = body["table"]
		a = new_term_search(db_name, search_term, lim, table, traverse=False)
		rr.rrAdapter("INFO","Performing search in the extensions",logDict)
# added****
		if table == "llt":
			print("in llt")
			a = terms_search(a,search_term,db_name, lim, table, traverse=False)
			rr.rrAdapter("INFO","Performing search in the llt table",logDict)
	else :
		table = "llt"
		a = new_term_search(db_name, search_term, lim, "llt")
		rr.rrAdapter("INFO","Performing search in the llt table",logDict)
# added***
		a = terms_search(a,search_term,db_name,  lim, "llt")
	rr.rrAdapter("INFO","Performed Search successfully",logDict)
	return a

@app.route("/searchlevel", methods = ['POST'])
def searcher():
	body = json.loads(request.data)
	db_name = body["clientId"]
	clientId = body["clientId"]
	search_id= body["search_id"]
	table = body["table"]
	version = body["version"]
	Id = request.headers.get("rrId")
	if Id is None:
		Id = str(uuid.uuid4())
	head = {"rrId":Id}
	logDict = {"rrId":Id,"source":"medictionary-service"}
	rr.rrAdapter("INFO","Request received for searching the ID in same level",logDict)
	db_name = db_name+"__"+version
	try:
		conn = psycopg2.connect("dbname="+db_name+" user="+DB_USER+" password="+DB_PASSWORD+" host="+DB_HOST+" port="+DB_PORT)
		conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT);
		cur = conn.cursor(cursor_factory = psycopg2.extras.DictCursor)
		rr.rrAdapter("INFO","Connected to client DB",logDict)
	except (Exception, psycopg2.DatabaseError) as error:
		print(error,flush=True)
		print("here",flush=True)
		rr.rrAdapter("ERROR","Failed to access client DB",logDict)
		return {"statusCode": 500, "message": "Failed to access client DB"}, 500
	tab_rels = tables[table]
	below_table = "N/A"
	below_ids=[]
	ret_val={}
	below_data = []
	if (table == "pt"):
		command = f"select * from llt where pt_code LIKE '%{search_id}%';"
		cur.execute(command)
		rr.rrAdapter("INFO","Selecting the search ID from llt",logDict)
		for i in cur:
			below_data.append(dict(i))
		# ret_val[below_table] = below_data
		rr.rrAdapter("INFO","Performed search successfully",logDict)
		return {"statusCode":200, "llt":below_data} #, f"{above_table}":above_data}
	if tab_rels["below"] is not None:
		below_table = tab_rels["below"].split("_")
		below_table = below_table[1]
		rr.rrAdapter("INFO","Searching the lower order table",logDict)

	# if above_table != "N/A":

	#     command = f"select * from {table} where {table}_code LIKE '%{search_id}%';"

	#     cur.execute(command)
	#     for i in cur:
	#         above_ids.append(i[f"{table}_code"])
	#     above_ids = list_for_query(above_ids)
	#     command = f"select * from {tab_rels['above']} where {table}_code in {above_ids};"
	#     print(command,flush=True)
	#     cur.execute(command)
	#     ids = []
	#     for i in cur:
	#         ids.append(i[f"{above_table}_code"])
	#         #if there is a need to associate with previous id, query here and structure accordingly
	#     ids = list_for_query(ids)
	#     command = f"select * from {above_table} where {above_table}_code in {ids};"
	#     cur.execute(command)

	#     for i in cur:
	#         above_data.append(dict(i))
	#     ret_val[above_table] = above_data



	if below_table is not "N/A":
		command = f"select * from {table} where {table}_code LIKE '%{search_id}%';"

		cur.execute(command)
		rr.rrAdapter("INFO","Selecting the search_id from table",logDict)
		for i in cur:
			below_ids.append(i[f"{table}_code"])
		below_ids = list_for_query(below_ids)
		command = f"select * from {tab_rels['below']} where {table}_code in {below_ids};"
		cur.execute(command)
		ids = []
		for i in cur:
			ids.append(i[f"{below_table}_code"])
			# if there is a need to associate with previous id, query here and structure accordingly
		ids = list_for_query(ids)
		command = f"select * from {below_table} where {below_table}_code in {ids};"
		cur.execute(command)

		for i in cur:
			below_data.append(dict(i))
		ret_val[below_table] = below_data
	cur.close()
	if len(below_data)==0:
		rr.rrAdapter("INFO","No data is found with the search_id",logDict)
		return {
			"statusCode":404,
			"message":"No data found"
		}, 404
	else:
		rr.rrAdapter("INFO","Search successful",logDict)
		return {"statusCode":200, f"{below_table}":below_data} #, f"{above_table}":above_data}
# @app.route("/access", methods = ["POST"])
# def _access_hai_kya():
#     body = json.loads(request.data)
#     paths = body["paths"]
#     a =db_initialiser(paths,"hi")
#     return a

@app.route("/search_individual", methods = ["POST"])
def individual_search():
	body = json.loads(request.data)
	db_name = body["clientId"]
	clientId = body["clientId"]
	search_term= body["search_term"]
	table = body["table"]
	version = body["version"]
	Id = request.headers.get("rrId")
	if Id is None:
		Id = str(uuid.uuid4())
	head = {"rrId":Id}
	logDict = {"rrId":Id,"source":"medictionary-service"}
	rr.rrAdapter("INFO","Request received for individual search",logDict)
	db_name = db_name+"__"+version
	try:
		conn = psycopg2.connect("dbname="+db_name+" user="+DB_USER+" password="+DB_PASSWORD+" host="+DB_HOST+" port="+DB_PORT)
		conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT);
		cur = conn.cursor(cursor_factory = psycopg2.extras.DictCursor)
		rr.rrAdapter("INFO","Connecting to client DB",logDict)
	except (Exception, psycopg2.DatabaseError) as error:
		print(error,flush=True)
		print("here",flush=True)
		rr.rrAdapter("ERROR","Failed to access client DB",logDict)
		return {"statusCode": 500, "message": "Failed to access client DB"}, 500
	command = f"select * from {table} where {table}_name ILIKE '%{search_term}%';"
	cur.execute(command)
	resj = []
	for res in cur:
		resj.append(dict(res))
	cur.close()
	if len(resj)>0:
		rr.rrAdapter("INFO","Searching the search_term in table",logDict)
		rr.rrAdapter("INFO","Search Successful",logDict)
		return {"statusCode":200, f"{table}":resj}
	else :
		rr.rrAdapter("INFO","Search_term not found in table",logDict)
		return {"statusCode":404, "message":"Keyword not found"}, 404

@app.route("/c_format/createdb",methods=['POST'])
def create_database():
	user_data = json.loads(request.data)
	folder_name = user_data.get('folderName', "./WhoDD")
	folder_to_extract_path = str(folder_name) + '/' + "Sample C Format"
	txt_to_csv(folder_to_extract_path,folder_name)
	Id = request.headers.get("rrId")
	if Id is None:
		Id = str(uuid.uuid4())
	head = {"rrId":Id}
	logDict = {"rrId":Id,"source":"medictionary-service"}
	rr.rrAdapter("INFO","Request received for creating DB",logDict)
# implemented postgresql
	db_name = user_data['clientId']
	#db_name = "whodd_c_format"
	create_db(db_name)
	create_tables(folder_to_extract_path,db_name)
	rr.rrAdapter("INFO","Database and tables successfully created",logDict)
	return {"message":"database and tables successfully created. Db-name : %s"%db_name, "statusCode":200}, 200

@app.route("/c_format/search", methods = ['POST'])
def search_api():
	user_data =json.loads(request.data)
	print('yesh',user_data)
	Id = request.headers.get("rrId")
	if Id is None:
		Id = str(uuid.uuid4())
	head = {"rrId":Id}
	logDict = {"rrId":Id,"source":"medictionary-service"}
	rr.rrAdapter("INFO","Request received for searching the DB based on id/text",logDict)
	if "clientId" not in user_data:
		return {"message" : "Mandatory field clientId missing", "statusCode":400}, 400
	body_copy = copy.deepcopy(user_data)
	body_copy.pop("clientId")
	body_copy.pop("userSession", None)
	if body_copy == {}:
		return {"message" : "Mandatory field(s) for search missing", "statusCode":400}, 400
	user_data = {k:v for k,v in user_data.items() if not isinstance(v, dict) and str(v).strip() != ""}
	search_by_id = [ "medicinalProdId","medId","atcCode"]
	search_by_text = [ "drugName","sourceText","substanceName","mahName"]
	keys = user_data.keys()
	if len(set(keys).intersection(search_by_id))>1:
		rr.rrAdapter("INFO","Only one search_id can be allowed to be searched at a time",logDict)
		return {"message":"search by id : ONLY ONE field allowed to be search at a time", "statusCode": 400}, 400
	elif  len(set(keys).intersection(search_by_text))>1:
		rr.rrAdapter("INFO","Only one search_by_text can be allowed to be searched at a time",logDict)
		return {"message":"search by text : ONLY ONE field allowed to be search at a time","statusCode": 400}, 400
	else:
		db_name = user_data['clientId']
		# db_name = "whodd_c_format2"
		final_results = joining_and_fetching_data(user_data,db_name)
		if "data" in final_results.keys():
			if 'drugName' in user_data.keys():
				search_term = user_data['drugName']
				rr.rrAdapter("INFO","Searching the drug name from db",logDict)
				final_results = terms_search_whodd(final_results,search_term,db_name,'c')
			else:
				final_results = final_results
			if len(final_results["data"]) < 1:
				rr.rrAdapter("INFO","Search Successful but 0 results",logDict)
				return {"data": final_results["data"], "statusCode": 200, "message": "Search successful but no matching results found"}
			rr.rrAdapter("INFO","Search Successful",logDict)
			return {"data": final_results["data"], "statusCode": 200, "message": "Search successful"}
		else:
			rr.rrAdapter("ERROR","ERROR occured while searching",logDict)
			return {"data": final_results, "statusCode": 200, "message": "Search successful NOT"}


# get drop down list items from db
@app.route("/c_format/list",methods = ['POST'])
def fetch_data_group_fields():
	body = json.loads(request.data)
	if "clientId" not in body or "what" not in body:
		return {"message" : "One or more mandatory fields [clientId, what] missing", "statusCode":400}, 400
	db_name= body.get('clientId')
	# db_name = "whodd_c_format2"
	what = body.get('what')
	Id = request.headers.get("rrId")
	if Id is None:
		Id = str(uuid.uuid4())
	head = {"rrId":Id}
	logDict = {"rrId":Id,"source":"medictionary-service"}
	rr.rrAdapter("INFO","Request received for fetching the data",logDict)
	what_lists = ['country','sourceCountry',"mahCountry","productGroup","productType","pharmacetucalForm","route"]
	resp = show_dropdown_data(db_name)
	if "statusCode" in resp and resp["statusCode"] == 400:
		return resp
	country_name,sourceCountry ,product_grp,productType,pf,mah_country,route_of_administrations= resp
	data = {"countryList":country_name,"sourceCountryList":sourceCountry,"mahCountryList":mah_country,"productGroupList":product_grp,"productTypeList":productType,"pharmaceuticalFormList":pf,"routeList":route_of_administrations}
	dict_keys = ['countryList','sourceCountryList' ,'mahCountryList','productGroupList','productTypeList','pharmaceuticalFormList','routeList']
	if what == "all":
		rr.rrAdapter("INFO","Successfully fetched the data",logDict)
		return {"data":data, "statusCode":200, "message":" All list returned"}
	elif what in what_lists:
		i = what_lists.index(what)
		data1 = data[dict_keys[i]]
		rr.rrAdapter("INFO","Successfully fetched the list",logDict)
		return {what+"List":data1, "statusCode":200, "message":what+ " list returned"}
	else:
		rr.rrAdapter("ERROR","Paramater not recognised, try again",logDict)
		return {"message":"Unrecognised parameter", "statusCode":400}, 400

# search by grp only (req params formdata)
# @app.route("/searchbygrp", methods = ['POST'])
# def search_api_grp():
#     body = json.loads(request.data)
# 	db_name= body.get('clientId')
# 	db_name = "whodd_c_format"
# 	what = body.get('what')
#     sourceCountry = request.form.get('sourceCountry')
#     mah_country = request.form.get('mahOrgCountry')
#     product_group = request.form.get('productGroupName')
#     productType=request.form.get('productTypeText')
#     pharmacetucal_form = request.form.get('pharmaceuticalFormText')
#     route = request.form.get('route')
#     db_name = request.form['db_name']
#     data_dict = {'country':country,'sourceCountry':sourceCountry,'mahOrgCountry':mah_country,'productGroupName':product_group,'productTypeText':productType,'pharmaceuticalFormText':pharmacetucal_form,'route':route}
#     print(data_dict)
#     user_data = {k:v for k,v in data_dict.items() if type(v)==str}
#     print(user_data,db_name)
#     final_results = joining_and_fetching_data(user_data,db_name)
#     return final_results



if __name__ == "__main__":
	#rr.setupLogHandler("MedDRA", "DEBUG")
	app.run(host="0.0.0.0", port=8008, debug=True)
