import psycopg2
import psycopg2.extras
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
import os
import uuid
# from rr_python import rr_python as rr

table_order = {
    "llt":"pt",
    "pt":"hlt",
    "hlt":"hlgt",
    "hlgt":"soc"
}

tables ={
    "llt":{
        "above":"pt",
        "below":None,
        "id_var":"llt_code"
    },
    "pt":{
        "above":"hlt_pt",
        "below":None,
        "id_var":"pt_code"
    },
    "hlt":{
        "above":"hlgt_hlt",
        "below":"hlt_pt",
        "id_var":"hlt_code"
    },
    "hlgt":{
        "above":"soc_hlgt",
        "below":"hlgt_hlt",
        "id_var":"hlgt_code"
    },
    "soc":{
        "above":None,
        "below":"soc_hlgt",
        "id_var":"soc_code"
    },
}
DB_HOST = os.environ.get('postgresHost',"localhost")
DB_PASSWORD=  os.environ.get('postgresPassword',"postgres")
DB_USER = os.environ.get('postgresUser',"postgres")
DB_PORT = os.environ.get('postgresPost',"5432")



def textcorrect(inp):
    s = str(inp[0]).upper()
    for c in inp[1:]:
        s = s + c
    return s
def return_list(rows):
    ret_l = []
    for er in rows:
        ret_l.append(dict(er))
    return ret_l

def search(cur, search_input, lim, table=None):
    return_results = []
    n_results = 0
    tab_spec=""
    if table != None:
        tab_spec = f"and table_name='{table.lower()}'"
    #search_input = str(search_input).lower()
    try:
        if(' ' not in search_input):
            query = f"SELECT * from index_1 WHERE term_name LIKE '"+textcorrect(search_input)+f"%' {tab_spec} LIMIT {lim}"
            cur.execute(query)
            return_results.append(return_list(cur.fetchall()))
            n_results =len(return_results[0])
            query = "SELECT * from index_1 WHERE term_name LIKE '%"+search_input+f"%' {tab_spec} LIMIT {lim}"
            cur.execute(query)
            return_results.append(return_list(cur.fetchall()))
            n_results =n_results+len(return_results[1])
            if(n_results <5):
                if tab_spec != "":
                    tab_spec = tab_spec.replace("and ", "")
                    query = "SELECT *, similarity(term_name, '"+search_input+f"') AS sim from index_1 WHERE {tab_spec} ORDER BY sim DESC LIMIT {lim};"
                else:
                    
                    query = "SELECT *, similarity(term_name, '"+search_input+f"') AS sim from index_1 ORDER BY sim DESC LIMIT {lim};"
                cur.execute(query)
                return_results.append(return_list(cur.fetchall()))
                n_results =n_results+len(return_results[2])
        else:
            query = "SELECT * from index_1 WHERE term_name LIKE '"+textcorrect(search_input)+f"%' {tab_spec} LIMIT {lim}"
            cur.execute(query)
            return_results.append(return_list(cur.fetchall()))
            n_results =n_results+len(return_results[0])
            if tab_spec == "":
                query = "SELECT *, similarity(term_name, '"+search_input+"') AS sim from index_1 ORDER BY sim DESC LIMIT {lim};"
            else:
                tab_spec = tab_spec.replace("and ", "")
                query = "SELECT *, similarity(term_name, '"+search_input+f"') AS sim from index_1 WHERE {tab_spec} ORDER BY sim DESC LIMIT {lim};"
            cur.execute(query)
            return_results.append(return_list(cur.fetchall()))
            n_results =n_results+len(return_results[1])
    
    except Exception as err:
        if err.pgcode and err.pgcode == "42P01":
            command = "create table index_1 (table_name VARCHAR(10), term_name VARCHAR(255), term_code VARCHAR(255));"
            cur.execute(command)
            command = "insert into index_1 (select 'hlgt' as table_name, hlgt_name as term_name, hlgt_code as term_code from hlgt);"
            cur.execute(command)
            command = "insert into index_1 (select 'hlt' as table_name, hlt_name as term_name, hlt_code as term_code from hlt);"
            cur.execute(command)
            command = "insert into index_1 (select 'pt' as table_name, pt_name as term_name, pt_code as term_code from pt);"
            cur.execute(command)
            command = "insert into index_1 (select 'llt' as table_name, llt_name as term_name, llt_code as term_code from llt);"
            cur.execute(command)
            command = "insert into index_1 (select 'soc' as table_name, soc_name as term_name, soc_code as term_code from soc);"
            cur.execute(command)
            return search(cur, lim, search_input)
        elif err.pgcode == "42883":
            command = "CREATE EXTENSION pg_trgm;"
            cur.execute(command)
            return search(cur, lim, search_input)
        else:
            print(err)
            print(err.pgcode)
            return {"statusCode":400, "message":"Something went wrong : "+str(err)}
    final_results=[]
    for gp in return_results:
        for rec in gp:
            obj={}
            obj[f"{rec['table_name']}_code"] = rec["term_code"]
            obj[f"{rec['table_name']}_name"] = rec["term_name"]
            obj["table"] = rec["table_name"]
            if obj not in final_results:
                final_results.append(obj)
                if len(final_results) == lim:
                    return {"data":final_results, "statusCode":200}

#     print(return_results)
    return {"data":final_results, "statusCode":200}

def new_term_search(db_name, term, lim, table=None, traverse=True):
    try:
        conn = psycopg2.connect("dbname="+db_name+" user="+DB_USER+" password="+DB_PASSWORD+" host="+DB_HOST+" port="+DB_PORT)
        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT);
        cur = conn.cursor(cursor_factory = psycopg2.extras.DictCursor)
    except (Exception, psycopg2.DatabaseError) as error:
        print(error,flush=True)
        return {"statusCode": 500, "message": "Failed to access client DB"}
    a = search(cur, term, lim, table)
    # print(a)
    if a["statusCode"] != 200:
        #rr.rrAdapter("warning", a["message"], {"rrId":str(uuid.uuid4()), "source":"Medictionary Service"})
        return a
    final_stuff=[]
    for i in a["data"]:
        tab = i["table"]
        if tab == "soc":
            final_stuff.extend(a["data"])
        else:  
            data=add_next(db_name, tab, [i])

            above=data["above"]
            core = data["data"]
            while(above != None):
                data=add_next(db_name, above, core)
                above=data["above"]
                core = data["data"]
                final_stuff.extend(core)
    # final_stuff2 = [itm for itm in final_stuff if itm not in final_stuff2]
    final_stuff2=[]
    for itm in final_stuff:
        if itm not in final_stuff2:
            final_stuff2.append(itm)

    #rr.rrAdapter("info", f"Found {str(len(final_stuff2))} records for given term", {"rrId":str(uuid.uuid4()), "source":"Medictionary Service"})
    return ({"statusCode":200, "data":final_stuff2})


def search_for_term(db_name, search_term, table, traverse):
    try:
        conn = psycopg2.connect("dbname="+db_name+" user="+DB_USER+" password="+DB_PASSWORD+" host="+DB_HOST+" port="+DB_PORT)
        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT);
        cur = conn.cursor(cursor_factory = psycopg2.extras.DictCursor)
    except (Exception, psycopg2.DatabaseError) as error:
        print(error,flush=True)
        #rr.rrAdapter("warning", error, {"rrId":str(uuid.uuid4()), "source":"Medictionary Service"})
        return {"statusCode": 500, "message": "Failed to access client DB"}
    try :
        command = f"select * from {table} where SIMILARITY({table}_name,'%{search_term}%') > 0.41 or {table}_name @@ '{search_term}';"
        cur.execute(command)
    except Exception as e:
        if ('pgcode' in dir(e) and e.pgcode == "42883"):
            print("Could not do SIMILARITY, creating extension pg_trgm coz extension was not found")
            #rr.rrAdapter("warning", "Could not do SIMILARITY, creating extension pg_trgm coz extension was not found", {"rrId":uuid.uuid4(), "source":"Medictionary Service"})
            command = "CREATE EXTENSION pg_trgm;"
            cur.execute(command)
            search_for_term(db_name, search_term, table, traverse)
        else :
            #rr.rrAdapter("warning", e, {"rrId":str(uuid.uuid4()), "source":"Medictionary Service"})
            return {"message":e, "statusCode":500}
    res=[]
    for ob in cur:
        res.append(dict(ob))
    if len(res)>0:
        return {"table":table, "data":res, "statusCode":200}
    elif traverse:
        if table != "soc":
            return search_for_term(db_name, search_term, table_order[table],traverse)
        else :
            return {"message":"Not found", "statusCode":400}
    else:
        return {"message":"Not found in that table", "statusCode":400}

    
def add_next(db_name, table, data):
    try:
        conn = psycopg2.connect("dbname="+db_name+" user="+DB_USER+" password="+DB_PASSWORD+" host="+DB_HOST+" port="+DB_PORT)
        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT);
        cur = conn.cursor(cursor_factory = psycopg2.extras.DictCursor)
    except (Exception, psycopg2.DatabaseError) as error:
        print(error,flush=True)
        print("here",flush=True)
        #rr.rrAdapter("warning",  "Failed to access client DB", {"rrId":str(uuid.uuid4()), "source":"Medictionary Service"})
        return {"statusCode": 500, "message": "Failed to access client DB"}, 500

    tab_rels = tables[table]
    above_table = "N/A"
    above_ids=[]
    above_data = []
    ret_list=[]
    if tab_rels["above"] is not None:
        above_table = tab_rels["above"].split("_")
        above_table= above_table[0]
    new_data = []
    if above_table != "N/A":
        for i in data:
            search_id = i[f"{table}_code"]
            if table == "llt":
                 command = f"select pt_code from llt where llt_code LIKE '%{search_id}%';"
            else:
                command = f"select {above_table}_code from {tab_rels['above']} where {table}_code LIKE '%{search_id}%';"
            cur.execute(command)
            for j in cur:
                above_data.append(j[f"{above_table}_code"])
            for dat in above_data:
                command = f"select * from {above_table} where {above_table}_code LIKE '%{dat}%'"
                cur.execute(command)
                for c in cur:
                    val = c[f"{above_table}_name"]
                    val_id = c[f"{above_table}_code"]
                    i[f"{above_table}_name"] = val
                    i[f"{above_table}_code"] = val_id

                    new_data.append(i)

        return {"above":above_table, "data":new_data}
    elif above_table == "N/A":
        return {"above":None, "data":data}
