ABWAruba                         
AFGAfghanistan                   
AGOAngola                        
AIAAnguilla                      
ALBAlbania                       
ANDAndorra                       
ANTNetherlands Antilles          
AREUnited Arab Emirates          
ARGArgentina                     
ARMArmenia                       
ASMAmerican Samoa                
ATAAntarctica                    
ATFFrench Southern Territories   
ATGAntigua and Barbuda           
AUSAustralia                     
AUTAustria                       
AZEAzerbaijan                    
BDIBurundi                       
BELBelgium                       
BENBenin                         
BFABurkina Faso                  
BGDBangladesh                    
BHRBahrain                       
BHSBahamas                       
BIHBosnia and Herzegovina        
BLBBelarus                       
BLZBelize                        
BMUBermuda                       
BOLBolivia                       
BRABrazil                        
BRBBarbados                      
BRNBrunei Darussalam             
BTNBhutan                        
BULBulgaria                      
BVTBouvet Island                 
BWABotswana                      
CAFCentral African Republic      
CANCanada                        
CCKCocos (Keeling) Islands       
CHLChile                         
CHNChina                         
CIVCote d'Ivoire                 
CMRCameroon                      
CODCongo, the Democratic Republic
COGCongo                         
COKCook Islands                  
COLColombia                      
COMComoros                       
CORCosta Rica                    
CPVCape Verde                    
CROCroatia                       
CUBCuba                          
CXRChristmas Island              
CYMCayman Islands                
CYPCyprus                        
CZECzech Republic                
DENDenmark                       
DJIDjibouti                      
DMADominica                      
DOMDominican Republic            
DZAAlgeria                       
ECUEcuador                       
EGYEgypt                         
ERIEritrea                       
ESHWestern Sahara                
ESTEstonia                       
ETHEthiopia                      
FINFinland                       
FJIFiji                          
FLKFalkland Islands (Malvinas)   
FRAFrance                        
FROFaroe Islands                 
FSMMicronesia, Federated States o
GABGabon                         
GDRGerman Democratic Republic    
GEOGeorgia                       
GFRGermany                       
GHAGhana                         
GIBGibraltar                     
GINGuinea                        
GLPGuadeloupe                    
GMBGambia                        
GNBGuinea-Bissau                 
GNQEquatorial Guinea             
GRCGreece                        
GRDGrenada                       
GRLGreenland                     
GTMGuatemala                     
GUFFrench Guiana                 
GUMGuam                          
GUYGuyana                        
HKGHong Kong                     
HMDHeard Island and McDonald Isla
HNDHonduras                      
HTIHaiti                         
HUNHungary                       
ICEIceland                       
INDIndia                         
INOIndonesia                     
IOTBritish Indian Ocean Territory
IREIreland                       
IRNIran, Islamic Republic of     
IRQIraq                          
ISRIsrael                        
ITAItaly                         
IWYMidway Islands                
JAMJamaica                       
JORJordan                        
JPNJapan                         
JTNJohnston Atoll                
KAZKazakhstan                    
KENKenya                         
KGZKyrgyzstan                    
KHMCambodia                      
KIRKiribati                      
KNASaint Kitts and Nevis         
KORKorea, Republic of            
KWTKuwait                        
LAOLao People's Democratic Republ
LBNLebanon                       
LBRLiberia                       
LBYLibyan Arab Jamahiriya        
LCASaint Lucia                   
LIELiechtenstein                 
LKASri Lanka                     
LSOLesotho                       
LTULithuania                     
LUXLuxembourg                    
LVALatvia                        
MACMacau                         
MALMalaysia                      
MDAMoldova, Republic of          
MDGMadagascar                    
MDVMaldives                      
MEXMexico                        
MHLMarshall Islands              
MKDMacedonia, the Former Yugoslav
MLIMali                          
MLTMalta                         
MMRMyanmar                       
MNGMongolia                      
MNPNorthern Mariana Islands      
MONMonaco                        
MORMorocco                       
MOZMozambique                    
MRTMauritania                    
MSRMontserrat                    
MTQMartinique                    
MUSMauritius                     
MWIMalawi                        
MYTMayotte                       
N/ANot Applicable                
NAMNamibia                       
NCLNew Caledonia                 
NERNiger                         
NETNetherlands                   
NEZNew Zealand                   
NFKNorfolk Island                
NGANigeria                       
NICNicaragua                     
NIUNiue                          
NORNorway                        
NPLNepal                         
NRUNauru                         
OMNOman                          
PAKPakistan                      
PANPanama                        
PCNPitcairn Islands              
PERPeru                          
PHLPhilippines                   
PLWPalau                         
PNGPapua New Guinea              
POLPoland                        
PORPortugal                      
PRIPuerto Rico                   
PRKKorea, Democratic People's Rep
PRYParaguay                      
PSEPalestinian Territory, Occupie
PYFFrench Polynesia              
QATQatar                         
REUReunion                       
ROMRomania                       
RUSRussian Federation            
RWARwanda                        
SAUSaudi Arabia                  
SCHSwitzerland                   
SDNSudan                         
SENSenegal                       
SGSSouth Georgia and the South Sa
SHNSaint Helena                  
SINSingapore                     
SJMSvalbard and Jan Mayen        
SLBSolomon Islands               
SLESierra Leone                  
SLVEl Salvador                   
SMRSan Marino                    
SOASouth Africa                  
SOMSomalia                       
SPASpain                         
SPMSaint Pierre and Miquelon     
STPSao Tome and Principe         
SURSuriname                      
SVKSlovakia                      
SVNSlovenia                      
SWESweden                        
SWZSwaziland                     
SYCSeychelles                    
SYRSyrian Arab Republic          
TANTanzania, United Republic of  
TCATurks and Caicos Islands      
TCDChad                          
TGOTogo                          
THAThailand                      
TJKTajikistan                    
TKLTokelau                       
TKMTurkmenistan                  
TMPEast Timor                    
TONTonga                         
TTOTrinidad and Tobago           
TUNTunisia                       
TURTurkey                        
TUVTuvalu                        
TWNTaiwan, Province of China     
UGAUganda                        
UKRUkraine                       
UMIUnited States Minor Outlying I
UNKUnited Kingdom                
UNSUnspecified                   
URYUruguay                       
USAUnited States                 
UZBUzbekistan                    
VATHoly See (Vatican City State) 
VCTSaint Vincent and the Grenadin
VENVenezuela                     
VGBVirgin Islands, British       
VIRVirgin Islands, U.S.          
VNMVietnam                       
VUTVanuatu                       
WLFWallis and Futuna             
WSMSamoa                         
YEMYemen                         
YUGSerbia and Montenegro         
ZMBZambia                        
ZWEZimbabwe                      
