001       Unspecified                                                                     
100       TABLETS                                                                         
101       TABLETS, TONGUE SOLUBLE                                                         
102       TABLETS, BUCCAL                                                                 
103       TABLETS, SUBLINGUAL                                                             
104       TABLETS, CHEWABLE                                                               
105       TABLETS, EFFERVESCENT                                                           
106       TABLETS, LAYERED                                                                
107       TABLETS, SOLUBLE                                                                
108       TABLETS, ENEMA                                                                  
109       TABLETS, OTHER                                                                  
110       TABLETS, COMBINATION PACK                                                       
125       COATED TABLETS                                                                  
126       COATED TABLETS, GELATIN                                                         
127       COATED TABLETS, FILM                                                            
128       COATED TABLETS, ENTERIC                                                         
129       COATED TABLETS, SUBLINGUAL                                                      
130       COATED TABLETS, CHEWABLE                                                        
131       COATED TABLETS, MEMBRANE                                                        
132       COATED TABLETS, OTHER                                                           
133       COATED TABLETS, COMBINATION PACK                                                
150       CAPSULES                                                                        
151       CAPSULES, ENTERIC-COATED                                                        
152       CAPSULES, BUCCAL                                                                
153       CAPSULES, BITING                                                                
154       CAPSULES, CHEWABLE                                                              
155       CAPSULES, MEMBRANE                                                              
156       CAPSULES, CACHETS                                                               
157       CAPSULES, INHALER                                                               
158       CAPSULES, FOR TOPICAL APPLICATION                                               
159       CAPSULES, OTHER                                                                 
160       CAPSULES, COMBINATION PACK                                                      
175       SPECIAL SOLID FORMS, PELLETS                                                    
176       SPECIAL SOLID FORMS, LOZENGES                                                   
177       SPECIAL SOLID FORMS, CHEWING GUM                                                
178       SPECIAL SOLID FORMS, SWEETS/CANDY/BONBONS                                       
179       SPECIAL SOLID FORMS, OCULAR THERAPEUTIC SYSTEMS                                 
180       SPECIAL SOLID FORMS, CUBES                                                      
181       SPECIAL SOLID FORMS, CAKES/CHOCOLATE/BARS/BISCUITS                              
182       SPECIAL SOLID FORMS, GLOBULI AND HOMEOPATHIC GLOBULI                            
183       SPECIAL SOLID FORMS, OTHER                                                      
184       SPECIAL SOLID FORMS, COMBINATION PACK                                           
200       POWDERS                                                                         
201       POWDERS, GRANULES                                                               
202       POWDERS, DUSTING                                                                
203       POWDERS, MEDICINAL/PHARMACEUTICAL SOLID SUBSTANCES                              
204       POWDERS, SUBLINGUAL POWDERS/GRANULES                                            
205       POWDERS, EFFERVESCENT POWDERS/GRANULES                                          
206       POWDERS, SOLUBLE                                                                
207       POWDERS, UNIT DOSE POWDER INHALER NON-REFILLABLE                                
208       POWDERS, UNIT DOSE POWDER INHALER REFILLABLE                                    
209       POWDERS, UNIT DOSE                                                              
210       POWDERS, ENEMA                                                                  
211       POWDERS, OTHER POWDERS/GRANULES                                                 
212       POWDERS, COMBINATION PACK POWDERS/GRANULES                                      
225       GASES, FOR INHALATION                                                           
250       LIQUIDS                                                                         
251       LIQUIDS, DROPS                                                                  
252       LIQUIDS, SPRAYS WITHOUT GAS                                                     
253       LIQUIDS, COLLODION/LACQUERS                                                     
254       LIQUIDS, INHALATIONS                                                            
255       LIQUIDS, SUBLINGUAL                                                             
256       LIQUIDS, OILS                                                                   
257       LIQUIDS, DRY SUSPENSIONS/SYRUPS/DROPS                                           
258       LIQUIDS, SUSPENSIONS                                                            
259       LIQUIDS, EMULSIONS                                                              
260       LIQUIDS, SYRUPS                                                                 
261       LIQUIDS, UNIT DOSE                                                              
262       LIQUIDS, METERED-DOSE                                                           
263       LIQUIDS, SPIRITS/WINES                                                          
264       LIQUIDS, LOTIONS                                                                
265       LIQUIDS, SHAKING MIXTURES FOR DERMATOLOGICAL USE                                
266       LIQUIDS, SOAPS/WASHES                                                           
267       LIQUIDS, ENEMA                                                                  
268       LIQUIDS, OTHER                                                                  
269       LIQUIDS, COMBINATION PACK                                                       
275       PRESSURISED AEROSOLS                                                            
276       PRESSURISED AEROSOLS, POWDERS                                                   
277       PRESSURISED AEROSOLS, BREATH-ACTUATED METERED-DOSE INHALER                      
278       PRESSURISED AEROSOLS, BREATH-ACTUATED CFC-FREE M-DOSE INH.                      
279       PRESSURISED AEROSOLS, METERED DOSE                                              
280       PRESSURISED AEROSOLS, CFC-FREE METERED-DOSE INHALERS                            
281       PRESSURISED AEROSOLS, OINTMENTS                                                 
282       PRESSURISED AEROSOLS, FOAMS                                                     
283       PRESSURISED AEROSOLS, SOAPS/WASHES                                              
284       PRESSURISED AEROSOLS, OTHER                                                     
285       PRESSURISED AEROSOLS, COMBINATION PACK                                          
300       BATHS                                                                           
301       BATHS, SOLID SUBSTANCES                                                         
302       BATHS, OILS                                                                     
303       BATHS, EMULSIONS                                                                
304       BATHS, PARTIAL                                                                  
305       BATHS, FOOT                                                                     
306       BATHS, FOAM                                                                     
307       BATHS, TABLETS                                                                  
308       BATHS, OTHER                                                                    
309       BATHS, COMBINATION PACK                                                         
325       TEAS                                                                            
326       TEAS, EXTRACT LIQUIDS                                                           
327       TEAS, INSTANT                                                                   
328       TEAS, BAGS                                                                      
329       TEAS, TABLETS                                                                   
330       TEAS, OTHER                                                                     
331       TEAS, COMBINATION PACK                                                          
350       SUPPOSITORIES                                                                   
351       SUPPOSITORIES, ADULT                                                            
352       SUPPOSITORIES, PAEDIATRIC                                                       
353       SUPPOSITORIES, VAGINAL                                                          
354       SUPPOSITORIES, MICRO ENEMAS                                                     
355       SUPPOSITORIES, OTHER                                                            
356       SUPPOSITORIES, COMBINATION PACK                                                 
375       AMPOULES                                                                        
376       AMPOULES, DRY                                                                   
377       AMPOULES, INTRAVENOUS                                                           
378       AMPOULES, INTRAMUSCULAR                                                         
379       AMPOULES, SUBCUTANEOUS                                                          
380       AMPOULES, INSTILLATION                                                          
381       AMPOULES, OTHER                                                                 
382       AMPOULES, COMBINATION PACK                                                      
400       PRE FILLED SYRINGES                                                             
401       PRE FILLED SYRINGES, DRY                                                        
402       PRE FILLED SYRINGES, INTRAVENOUS                                                
403       PRE FILLED SYRINGES, INTRAMUSCULAR                                              
404       PRE FILLED SYRINGES, SUBCUTANEOUS                                               
405       PRE FILLED SYRINGES, PENS                                                       
406       PRE FILLED SYRINGES, DRY PENS                                                   
407       PRE FILLED SYRINGES, INSTILLATION                                               
408       PRE FILLED SYRINGES, OTHER                                                      
409       PRE FILLED SYRINGES, COMBINATION PACK                                           
425       VIALS                                                                           
426       VIALS, DRY                                                                      
427       VIALS, INTRAVENOUS                                                              
428       VIALS, INTRAMUSCULAR                                                            
429       VIALS, SUBCUTANEOUS                                                             
430       VIALS, CARTRIDGES                                                               
431       VIALS, DRY CARTRIDGES                                                           
432       VIALS, UNIT DOSE CARTRIDGES                                                     
433       VIALS, INSTILLATION                                                             
434       VIALS, OTHER                                                                    
435       VIALS, COMBINATION PACK                                                         
450       INFUSION AMPOULES                                                               
451       INFUSION AMPOULES, DRY                                                          
452       INFUSION AMPOULES, VIALS/BOTTLES                                                
453       INFUSION AMPOULES, DRY VIALS/BOTTLES                                            
454       INFUSION AMPOULES, BAGS                                                         
455       INFUSION AMPOULES, CARTRIDGES                                                   
456       INFUSION AMPOULES, DIALYSIS IRRIGATION AND PERFUSION SOLU.                      
457       INFUSION AMPOULES, OTHER                                                        
475       OINTMENTS                                                                       
476       OINTMENTS, PASTES                                                               
477       OINTMENTS, EMULSION                                                             
478       OINTMENTS, UNIT DOSE OINTMENTS AND PASTES                                       
479       OINTMENTS, SOAP PASTES                                                          
480       OINTMENTS, OTHER                                                                
481       OINTMENTS, COMBINATION PACK                                                     
500       CREAMS                                                                          
501       CREAMS, EMULSION                                                                
502       CREAMS, UNIT DOSE                                                               
503       CREAMS, SOAPS /WASHES                                                           
504       CREAMS, OTHER                                                                   
505       CREAMS, COMBINATION PACK                                                        
525       GELS AND SOLS                                                                   
526       GELS AND SOLS, GEL DROPS                                                        
527       GELS AND SOLS, EMULSION GELS                                                    
528       GELS AND SOLS, UNIT DOSE GELS/SOLS                                              
529       GELS AND SOLS, WASH/SHOWER GELS                                                 
530       GELS AND SOLS, OTHER                                                            
531       GELS AND SOLS, COMBINATION PACK                                                 
550       MEDICATED DRESSINGS, PLASTERS WITH SUBSTANCE                                    
551       MEDICATED DRESSINGS, COTTON WITH SUBSTANCE                                      
552       MEDICATED DRESSINGS, GAUZE OR FLEECE WITH SUBSTANCE                             
553       MEDICATED DRESSINGS, PADS WITH SUBSTANCE                                        
554       MEDICATED DRESSINGS, TAMPONS WITH SUBSTANCE                                     
555       MEDICATED DRESSINGS, BANDAGES WITH SUBSTANCE                                    
556       MEDICATED DRESSINGS, SPONGES WITH SUBSTANCE                                     
557       MEDICATED DRESSINGS, TRANSDERMAL PATCHES                                        
558       MEDICATED DRESSINGS, POULTICES                                                  
559       MEDICATED DRESSINGS, OTHER                                                      
560       MEDICATED DRESSINGS, COMBINATION PACK                                           
575       OTHER SPECIAL FORMS - FOOD READY TO EAT                                         
576       OTHER SPECIAL FORMS - FOOD NOT READY TO EAT                                     
577       OTHER SPECIAL FORMS - CIGARETTES                                                
578       OTHER SPECIAL FORMS - STYLI AND WOUND CONES NON-RECTAL                          
579       OTHER SPECIAL FORMS - STICKS AND ROLL ONS                                       
580       OTHER SPECIAL FORMS - BONE CEMENTS WITH SUBSTANCE                               
581       OTHER SPECIAL FORMS - MECHANICAL PESSARIES WITH SUBSTANCE                       
582       OTHER SPECIAL FORMS - INTRA UTERINE DEVICES                                     
583       OTHER SPECIAL FORMS - FUMIGATION CANDLES                                        
584       OTHER SPECIAL FORMS - FOAMS                                                     
585       OTHER SPECIAL FORMS - IMPLANTS                                                  
586       OTHER SPECIAL FORMS - SOLID SOAPS/BARS                                          
587       OTHER SPECIAL FORMS - PROMOTIONAL PACKS                                         
588       OTHER SPECIAL FORMS - OTHER                                                     
589       OTHER SPECIAL FORMS - COMBINATION PACK                                          
600       MEDICAL AIDS - PLASTERS WITHOUT SUBSTANCE                                       
601       MEDICAL AIDS - COTTON WITHOUT SUBSTANCE                                         
602       MEDICAL AIDS - GAUZE OR FLEECE WITHOUT SUBSTANCE                                
603       MEDICAL AIDS - PADS WITHOUT SUBSTANCE                                           
604       MEDICAL AIDS - TAMPONS WITHOUT SUBSTANCE                                        
605       MEDICAL AIDS - BANDAGES WITHOUT SUBSTANCE                                       
606       MEDICAL AIDS - SPONGES WITHOUT SUBSTANCE                                        
607       MEDICAL AIDS - TISSUE ABSORBABLE MEDICAL AIDS                                   
608       MEDICAL AIDS - GEL AND COLLOID DRESSINGS                                        
609       MEDICAL AIDS - BONE CEMENTS WITHOUT SUBSTANCE                                   
610       MEDICAL AIDS - MECHANICAL PESSARIES WITHOUT SUBSTANCE                           
611       MEDICAL AIDS - DIAGNOSTIC STICKS                                                
612       MEDICAL AIDS - DIAGNOSTIC TESTS EXCLUDING STICKS                                
613       MEDICAL AIDS - OTHER                                                            
614       MEDICAL AIDS - COMBINATION PACK                                                 
900       Combination                                                                     
