#!/bin/bash
sed "s/latest/$1/g" deployment.yaml > dictionary-service.yaml
sed "s/latest/$1/g" dev-deployment.yaml > dev-dictionary-service.yaml
sed "s/latest/$1/g" sb-deployment.yaml > sb-dictionary-service.yaml
sed "s/latest/$1/g" qa-deployment.yaml > qa-dictionary-service.yaml